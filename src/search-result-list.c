// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include <gio/gio.h>

#include "search-result-list.h"
#include "search-result.h"

struct _SearchResultList
{
  GObject parent_instance;

  /* It would be nice to subclass this type, but it's declared "final" */
  GListStore *store;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (SearchResultList,
                               search_result_list,
                               G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                      list_model_iface_init))

static void
on_store_items_changed (GListModel *list,
                        guint position,
                        guint removed,
                        guint added,
                        gpointer user_data)
{
  SearchResultList *self = SEARCH_RESULT_LIST (user_data);

  g_list_model_items_changed (G_LIST_MODEL (self), position, removed, added);
}

SearchResultList *
search_result_list_new (void)
{
  return g_object_new (SEARCH_TYPE_RESULT_LIST, NULL);
}

static void
search_result_list_finalize (GObject *object)
{
  SearchResultList *self = (SearchResultList *) object;

  g_clear_object (&self->store);

  G_OBJECT_CLASS (search_result_list_parent_class)->finalize (object);
}

static void
search_result_list_class_init (SearchResultListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = search_result_list_finalize;
}

static void
search_result_list_init (SearchResultList *self)
{
  self->store = g_list_store_new (SEARCH_TYPE_RESULT);

  g_signal_connect_object (self->store, "items-changed", G_CALLBACK (on_store_items_changed), self, G_CONNECT_DEFAULT);
}

static gpointer
get_item (GListModel *list_model, guint position)
{
  SearchResultList *self = SEARCH_RESULT_LIST (list_model);

  return g_list_model_get_item (G_LIST_MODEL (self->store), position);
}

static GType
get_item_type (GListModel *list_model)
{
  return SEARCH_TYPE_RESULT;
}

static guint
get_n_items (GListModel *list_model)
{
  SearchResultList *self = SEARCH_RESULT_LIST (list_model);

  return g_list_model_get_n_items (G_LIST_MODEL (self->store));
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item = get_item;
  iface->get_item_type = get_item_type;
  iface->get_n_items = get_n_items;
}

/**
 * search_result_list_append:
 * @self: a #SearchResultList
 * @item: a #SearchResult
 *
 * Add @item to the list of search results. This takes a new ref on @item.
 */
void
search_result_list_append (SearchResultList *self, SearchResult *item)
{
  g_return_if_fail (SEARCH_IS_RESULT_LIST (self));
  g_return_if_fail (SEARCH_IS_RESULT (item));

  g_list_store_append (self->store, item);
}

/**
 * search_result_list_insert:
 * @self: a #SearchResultList
 * @position: the position to insert the result, starting at 0
 * @item: a #SearchResult
 *
 * Insert @item to the list of search results. This takes a new ref on @item.
 */
void
search_result_list_insert (SearchResultList *self, guint position, SearchResult *item)
{
  g_return_if_fail (SEARCH_IS_RESULT_LIST (self));
  g_return_if_fail (SEARCH_IS_RESULT (item));

  g_list_store_insert (self->store, position, item);
}

/**
 * search_result_list_remove:
 * @self: a #SearchResultList
 * @position: the position to remove the result, starting at 0
 *
 * Remove item at @position from the list of search results.
 */
void
search_result_list_remove (SearchResultList *self, guint position)
{
  g_return_if_fail (SEARCH_IS_RESULT_LIST (self));

  g_list_store_remove (self->store, position);
}

/**
 * search_result_list_remove_all:
 * @self: a #SearchResultList
 *
 * Remove all items.
 */
void
search_result_list_remove_all (SearchResultList *self)
{
  g_return_if_fail (SEARCH_IS_RESULT_LIST (self));

  if (g_list_model_get_n_items (G_LIST_MODEL (self->store)) > 0)
    {
      g_list_store_remove_all (self->store);
    }
}
