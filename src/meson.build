gnome = import('gnome')

libgnomesearch_sources = [
    'libgnomesearch.c',
    'search-context.c',
    'search-dbus-helpers.c',
    'search-dbus-worker.c',
    'search-error.c',
    'search-group.c',
    'search-provider.c',
    'search-provider-dbus.c',
    'search-query.c',
    'search-result.c',
    'search-result-icon-data.c',
    'search-result-list.c',
]

libgnomesearch_public_headers = [
    'libgnomesearch.h',
    'search-context.h',
    'search-enums.h',
    'search-error.h',
    'search-group.h',
    'search-provider.h',
    'search-provider-dbus.h',
    'search-query.h',
    'search-result.h',
    'search-result-icon-data.h',
    'search-result-list.h',
]

libgnomesearch_headers = libgnomesearch_public_headers + [
    'search-dbus-helpers.h',
    'search-dbus-worker.h',
]

enum_files = gnome.mkenums_simple('enum-types',
  sources: libgnomesearch_headers,
)

version_split = meson.project_version().split('.')
MAJOR_VERSION = version_split[0]
MINOR_VERSION = version_split[1]
MICRO_VERSION = version_split[2]

version_conf = configuration_data()
version_conf.set('VERSION', meson.project_version())
version_conf.set('MAJOR_VERSION', MAJOR_VERSION)
version_conf.set('MINOR_VERSION', MINOR_VERSION)
version_conf.set('MICRO_VERSION', MICRO_VERSION)

configure_file(
    input: 'libgnomesearch-version.h.in',
    output: 'libgnomesearch-version.h',
    configuration: version_conf,
    install: true,
    install_dir: join_paths(get_option('includedir'), 'libgnomesearch'),
)


libgnomesearch_deps = [
    glib_dep,
]

libgnomesearch = library(
    'gnomesearch-@0@'.format(api_version),
    libgnomesearch_sources + enum_files,
    dependencies: libgnomesearch_deps,
    install: true,
)

if get_option('default_library') == 'shared'
    libgnomesearch_static = static_library(
        'gnomesearch-@0@'.format(api_version),
        objects: libgnomesearch.extract_all_objects(recursive: true),
    )
elif get_option('default_library') == 'static'
    libgnomesearch_static = libgnomesearch
elif get_option('default_library') == 'both'
    libgnomesearch_static = libgnomesearch.get_static_lib()
endif

install_headers(libgnomesearch_public_headers, subdir: 'libgnomesearch')

pkg = import('pkgconfig')

pkg.generate(
    description: 'A shared library for desktop search',
    libraries: libgnomesearch,
    name: 'libgnomesearch',
    filebase: 'libgnomesearch-' + api_version,
    version: meson.project_version(),
    subdirs: 'libgnomesearch',
    requires: 'glib-2.0',
    install_dir: join_paths(get_option('libdir'), 'pkgconfig'),
)

libgnomesearch_dep = declare_dependency(
    link_with: libgnomesearch,
    include_directories: include_directories('.'),
    dependencies: libgnomesearch_deps,
)

libgnomesearch_static_dep = declare_dependency(
    link_whole: libgnomesearch_static,
    include_directories: include_directories('.'),
    dependencies: libgnomesearch_deps,
)

meson.override_dependency('libgnomesearch-' + api_version, libgnomesearch_dep)

libgnomesearch_gir = gnome.generate_gir(
    libgnomesearch,
    nsversion: api_version,
    namespace: 'Search',
    symbol_prefix: 'search',
    identifier_prefix: 'Search',
    includes: ['Gio-2.0'],
    sources: libgnomesearch_headers + libgnomesearch_sources,
    install: true,
    install_dir_typelib: typelibdir,
    #fatal_warnings: true,
    #extra_args: ['--c-include=libgnomesearch.h']
)
