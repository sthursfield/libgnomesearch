// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define SEARCH_TYPE_RESULT_ICON_DATA (search_result_icon_data_get_type ())

typedef struct _SearchResultIconData SearchResultIconData;

GType                     search_result_icon_data_get_type (void) G_GNUC_CONST;
SearchResultIconData     *search_result_icon_data_new_from_variant (GVariant *variant);
SearchResultIconData     *search_result_icon_data_copy     (SearchResultIconData *self);
void                      search_result_icon_data_free     (SearchResultIconData *self);

gint32 search_result_icon_data_get_width (SearchResultIconData *self);
gint32 search_result_icon_data_get_height (SearchResultIconData *self);
gint32 search_result_icon_data_get_rowstride (SearchResultIconData *self);
gboolean search_result_icon_data_get_has_alpha (SearchResultIconData *self);
gint32 search_result_icon_data_get_bits_per_sample (SearchResultIconData *self);
gint32 search_result_icon_data_get_channels (SearchResultIconData *self);
GBytes *search_result_icon_data_get_data (SearchResultIconData *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SearchResultIconData, search_result_icon_data_free)


G_END_DECLS
