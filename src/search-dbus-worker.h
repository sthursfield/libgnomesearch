// SPDX-FileCopyrightText: 2023  Kaspar Matas
// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <gio/gio.h>
#include <glib.h>

#include "search-enums.h"
#include "search-query.h"
#include "search-result-list.h"

/* Internal struct used to initialize the D-Bus worker. */

typedef struct
{
  GDBusConnection *dbus_connection;
  guint version;
  gchar *bus_name;
  gchar *object_path;
  guint dbus_timeout_ms;
  gchar *provider_name;
} SearchDBusWorkerConfig;

typedef enum
{
  SEARCH_DBUS_WORKER_MESSAGE_STARTED = 1,
  SEARCH_DBUS_WORKER_MESSAGE_STOPPED = 2,
  SEARCH_DBUS_WORKER_MESSAGE_STOP = 3,
  SEARCH_DBUS_WORKER_MESSAGE_SET_QUERY = 4,
} SearchDBusWorkerMessageType;

extern const gchar *_search_dbus_worker_message_name[];

typedef struct
{
  SearchStopReason stop_reason;
  gdouble elapsed_time;
  GError *error;
} SearchDBusWorkerStoppedData;

typedef struct
{
  SearchQuery *query;
} SearchDBusWorkerSetQueryData;

typedef struct
{
  SearchDBusWorkerMessageType message_type;
  union
  {
    void *empty_data;
    SearchDBusWorkerStoppedData stopped_data;
    SearchDBusWorkerSetQueryData set_query_data;
  };
} SearchDBusWorkerMessage;

typedef void (*SearchDBusWorkerMessageReadyCallback) (gpointer user_data);

SearchDBusWorkerConfig *_search_dbus_worker_config_new (GDBusConnection *dbus_connection,
                                                        guint version,
                                                        const gchar *bus_name,
                                                        const gchar *object_path,
                                                        guint dbus_timeout,
                                                        const gchar *provider_name);
void _search_dbus_worker_config_free (SearchDBusWorkerConfig *config);

GThread *_search_dbus_worker_thread_start (GMainContext *main_context,
                                           GAsyncQueue *rx,
                                           GAsyncQueue *tx,
                                           GCancellable *cancellable,
                                           SearchQuery *query,
                                           SearchResultList *result_list,
                                           GTimeSpan retrigger_interval,
                                           SearchDBusWorkerConfig *dbus_config,
                                           SearchDBusWorkerMessageReadyCallback message_ready_cb,
                                           GObject *message_receiver,
                                           GError **error);
