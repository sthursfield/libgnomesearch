// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef __SEARCH_ERROR_H__
#define __SEARCH_ERROR_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * SearchError:
 * @SEARCH_ERROR_LAST: The total number of error codes.
 *
 * Error domain for libgnomesearch. Errors in this domain will be from the
 * [error@Search.Error] enumeration. See [struct@GLib.Error] for more information on error
 * domains.
 */
typedef enum {
	SEARCH_ERROR_LAST
} SearchError;

#define SEARCH_N_ERRORS SEARCH_ERROR_LAST
#define SEARCH_ERROR (search_error_quark ())

GQuark search_error_quark (void);

G_END_DECLS

#endif
