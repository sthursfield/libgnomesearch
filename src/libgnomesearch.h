/* libgnomesearch.h
 *
 * Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define LIBGNOMESEARCH_INSIDE
#include "libgnomesearch-version.h"
#undef LIBGNOMESEARCH_INSIDE

#include "search-context.h"
#include "search-error.h"
#include "search-group.h"
#include "search-provider-dbus.h"
#include "search-provider.h"
#include "search-query.h"
#include "search-result.h"

G_END_DECLS
