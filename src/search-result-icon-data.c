// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include "search-result-icon-data.h"

#include <glib.h>

G_DEFINE_BOXED_TYPE (SearchResultIconData, search_result_icon_data, search_result_icon_data_copy, search_result_icon_data_free)

/**
 * SearchResultIconData:
 *
 * Holds raw icon data returned by a search provider. Providers have the option
 * of returning either a a serialized #GIcon or the raw image data.
 *
 * If you are using GTK and want to display the image data, use
 * something like the following example (in C):
 *
 * ```
 * GdkPixbuf *image = gdk_pixbuf_new_from_bytes (search_result_icon_data_get_bytes (icon_data)
 *                                               GDK_COLORSPACE_RGB,
 *                                               search_result_icon_data_get_has_alpha (icon_data),
 *                                               search_result_icon_data_get_bits_per_sample (icon_data),
 *                                               search_result_icon_data_get_width (icon_data),
 *                                               search_result_icon_data_get_height (icon_data),
 *                                               search_result_icon_data_get_rowstride (icon_data));
 * ```
 */
typedef struct _SearchResultIconData {
  gint32 width;
  gint32 height;
  gint32 rowstride;
  gboolean has_alpha;
  gint32 bits_per_sample;
  gint32 channels;
  GBytes *data;
} SearchResultIconData;

static void
init_from_variant (SearchResultIconData *data, GVariant *variant)
{
  gchar *image_data;
  gsize image_data_size;

  g_variant_get (variant,
                 "iiibii^ay",
                 &data->width,
                 &data->height,
                 &data->rowstride,
                 &data->has_alpha,
                 &data->bits_per_sample,
                 &data->channels,
                 &image_data);

  image_data_size = strlen (image_data);
  data->data = g_bytes_new_take (image_data, image_data_size);
}

/**
 * search_result_icon_data_new_from_variant:
 * @variant: A #GVariant, as returned by org.gnome.Shell.SearchProvider.getResultMetas D-Bus method.
 *
 * Creates a new #SearchResultIconData, or returns %NULL if the input variant
 * does not follow the expected type.
 *
 * Returns: (transfer full): A newly created #SearchResultIconData
 */
SearchResultIconData *
search_result_icon_data_new_from_variant (GVariant *variant)
{
  SearchResultIconData *self;

  g_return_val_if_fail (!g_variant_is_of_type (variant, G_VARIANT_TYPE ("iiibiiay")), NULL);

  self = g_slice_new0 (SearchResultIconData);

  init_from_variant (self, variant);

  return self;
}

/**
 * search_result_icon_data_copy:
 * @self: a #SearchResultIconData
 *
 * Makes a deep copy of a #SearchResultIconData.
 *
 * Returns: (transfer full): A newly created #SearchResultIconData with the same
 *   contents as @self
 */
SearchResultIconData *
search_result_icon_data_copy (SearchResultIconData *self)
{
  SearchResultIconData *copy;

  g_return_val_if_fail (self, NULL);

  copy = g_new0 (SearchResultIconData, 1);
  copy->width = self->width;
  copy->height = self->height;
  copy->rowstride = self->rowstride;
  copy->has_alpha = self->has_alpha;
  copy->bits_per_sample = self->bits_per_sample;
  copy->channels = self->channels;
  copy->data = g_bytes_ref (self->data);

  return copy;
}

/**
 * search_result_icon_data_free:
 * @self: a #SearchResultIconData
 *
 * Frees a #SearchResultIconData allocated using search_result_icon_data_new()
 * or search_result_icon_data_copy().
 */
void
search_result_icon_data_free (SearchResultIconData *self)
{
  g_return_if_fail (self);

  g_clear_pointer (&self->data, g_bytes_unref);
  g_slice_free (SearchResultIconData, self);
}

/**
 * search_result_icon_data_get_width:
 * @self: a #SearchResultIconData struct
 *
 * Returns: the width of the icon data
 */
gint32
search_result_icon_data_get_width (SearchResultIconData *self)
{
  return self->width;
}

/**
 * search_result_icon_data_get_height:
 * @self: a #SearchResultIconData struct
 *
 * Returns: the height of the icon data
 */
gint32
search_result_icon_data_get_height (SearchResultIconData *self)
{
  return self->height;
}

/**
 * search_result_icon_data_get_rowstride:
 * @self: a #SearchResultIconData struct
 *
 * Returns: the rowstride of the icon data
 */
gint32
search_result_icon_data_get_rowstride (SearchResultIconData *self)
{
  return self->rowstride;
}

/**
 * search_result_icon_data_get_has_alpha:
 * @self: a #SearchResultIconData struct
 *
 * Returns: %TRUE if the image data has an alpha channel
 */
gboolean
search_result_icon_data_get_has_alpha (SearchResultIconData *self)
{
  return self->has_alpha;
}

/**
 * search_result_icon_data_get_bits_per_sample:
 * @self: a #SearchResultIconData struct
 *
 * Returns: the bits_per_sample of the icon data
 */
gint32
search_result_icon_data_get_bits_per_sample (SearchResultIconData *self)
{
  return self->bits_per_sample;
}

/**
 * search_result_icon_data_get_channels:
 * @self: a #SearchResultIconData struct
 *
 * Returns: the number of channels channels of the icon data
 */
gint32
search_result_icon_data_get_channels (SearchResultIconData *self)
{
  return self->channels;
}

/**
 * search_result_icon_data_get_data:
 * @self: a #SearchResultIconData struct
 *
 * Returns: (transfer none): the image data, as a #GBytes structure
 */
GBytes *
search_result_icon_data_get_data (SearchResultIconData *self)
{
  return self->data;
}
