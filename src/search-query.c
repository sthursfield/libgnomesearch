// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

/**
 * SearchQuery:
 *
 * This #GBoxed type represents simple and complex search queries.
 *
 * ## Text queries
 *
 * The simplest kind of query matches a text string against all searchable
 * content.
 */

#include "search-query.h"

G_DEFINE_BOXED_TYPE (SearchQuery, search_query, search_query_copy, search_query_free)

typedef enum _SearchQueryKind
{
  SEARCH_QUERY_KIND_TEXT = 0,
} SearchQueryKind;

struct SearchQueryTextData
{
  char *text;
};

struct _SearchQuery
{
  SearchQueryKind kind;

  union
  {
    struct SearchQueryTextData text_data;
  };
};

/**
 * search_query_new_text:
 * @text: a string
 *
 * Creates a new #SearchQuery that matches a simple text string.
 *
 * Returns: (transfer full): A newly created #SearchQuery
 */
SearchQuery *
search_query_new_text (const char *text)
{
  SearchQuery *self;

  self = g_slice_new0 (SearchQuery);
  self->kind = SEARCH_QUERY_KIND_TEXT;
  self->text_data.text = g_strdup (text);

  return self;
}

/**
 * search_query_copy:
 * @self: a #SearchQuery
 *
 * Makes a deep copy of a #SearchQuery.
 *
 * Returns: (transfer full): A newly created #SearchQuery with the same
 *   contents as @self
 */
SearchQuery *
search_query_copy (SearchQuery *self)
{
  SearchQuery *copy;

  g_return_val_if_fail (self, NULL);

  copy = g_slice_new0 (SearchQuery);
  copy->kind = self->kind;

  switch (self->kind)
    {
    case SEARCH_QUERY_KIND_TEXT:
      copy->text_data.text = g_strdup (self->text_data.text);
      break;
    default:
      g_assert_not_reached ();
    }

  return copy;
}

/**
 * search_query_free:
 * @self: a #SearchQuery
 *
 * Frees a #SearchQuery allocated using search_query_new()
 * or search_query_copy().
 */
void
search_query_free (SearchQuery *self)
{
  g_return_if_fail (self);

  switch (self->kind)
    {
    case SEARCH_QUERY_KIND_TEXT:
      g_free (self->text_data.text);
      break;
    default:
      g_assert_not_reached ();
    }

  g_slice_free (SearchQuery, self);
}

/**
 * search_query_text_get_text:
 * @self: a #SearchQuery of kind #SEARCH_QUERY_KIND_TEXT
 *
 * Returns: a string, owned by the query.
 */
const gchar *
search_query_text_get_text (SearchQuery *self)
{
  g_return_val_if_fail (self->kind == SEARCH_QUERY_KIND_TEXT, NULL);

  return self->text_data.text;
}

gboolean
search_query_is_subquery (SearchQuery *first, SearchQuery *second)
{
  /* FIXME - test if text of 'first' contains 'second' */
  return FALSE;
}
