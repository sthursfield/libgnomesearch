// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define SEARCH_TYPE_QUERY (search_query_get_type ())

typedef struct _SearchQuery SearchQuery;

GType search_query_get_type (void) G_GNUC_CONST;
SearchQuery *search_query_new_text (const char *text);
SearchQuery *search_query_copy (SearchQuery *self);
void search_query_free (SearchQuery *self);

const gchar *search_query_text_get_text (SearchQuery *self);

gboolean search_query_is_subquery (SearchQuery *first, SearchQuery *second);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SearchQuery, search_query_free)

G_END_DECLS
