// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include "search-result.h"
#include "search-result-icon-data.h"

struct _SearchResult
{
  GObject parent_instance;

  gchar *id;
  gchar *name;
  GIcon *icon;
  SearchResultIconData *icon_data;
  gchar *description;
  gchar *clipboard_text;
  gchar *provider_name;
};

G_DEFINE_FINAL_TYPE (SearchResult, search_result, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_ICON,
  PROP_ICON_DATA,
  PROP_DESCRIPTION,
  PROP_CLIPBOARD_TEXT,
  PROP_PROVIDER_NAME,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

SearchResult *
search_result_new (const gchar *id,
                   const gchar *name,
                   GIcon *icon,
                   SearchResultIconData *icon_data,
                   const gchar *description,
                   const gchar *clipboard_text,
                   const gchar *provider_name)
{
  SearchResult *self;

  self = g_object_new (SEARCH_TYPE_RESULT,
                       "id", id,
                       "name", name,
                       "description", description,
                       "clipboard-text", clipboard_text,
                       "provider-name", provider_name,
                       NULL);
  if (icon)
    {
      g_object_set (self, "icon", icon, NULL);
    }

  if (icon_data)
    {
      g_object_set (self, "icon-data", icon_data, NULL);
    }

  return self;
}

static void
search_result_finalize (GObject *object)
{
  SearchResult *self = (SearchResult *) object;

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_object (&self->icon);
  g_clear_pointer (&self->icon_data, search_result_icon_data_free);
  g_clear_pointer (&self->description, g_free);
  g_clear_pointer (&self->clipboard_text, g_free);
  g_clear_pointer (&self->provider_name, g_free);

  G_OBJECT_CLASS (search_result_parent_class)->finalize (object);
}

static void
search_result_get_property (GObject *object,
                            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec)
{
  SearchResult *self = SEARCH_RESULT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    case PROP_ICON:
      g_value_set_object (value, self->icon);
      break;
    case PROP_ICON_DATA:
      g_value_set_boxed (value, self->icon_data);
      break;
    case PROP_DESCRIPTION:
      g_value_set_string (value, self->description);
      break;
    case PROP_CLIPBOARD_TEXT:
      g_value_set_string (value, self->clipboard_text);
      break;
    case PROP_PROVIDER_NAME:
      g_value_set_string (value, self->provider_name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_result_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
  SearchResult *self = SEARCH_RESULT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_assert (self->id == NULL);
      self->id = g_value_dup_string (value);
      break;
    case PROP_NAME:
      g_assert (self->name == NULL);
      self->name = g_value_dup_string (value);
      break;
    case PROP_ICON:
      g_clear_object (&self->icon);
      self->icon = g_value_dup_object (value);
      break;
    case PROP_ICON_DATA:
      g_clear_pointer (&self->icon_data, search_result_icon_data_free);
      self->icon = g_value_dup_boxed (value);
      break;
    case PROP_DESCRIPTION:
      g_assert (self->description == NULL);
      self->description = g_value_dup_string (value);
      break;
    case PROP_CLIPBOARD_TEXT:
      g_assert (self->clipboard_text == NULL);
      self->clipboard_text = g_value_dup_string (value);
      break;
    case PROP_PROVIDER_NAME:
      g_assert (self->provider_name == NULL);
      self->provider_name = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_result_class_init (SearchResultClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = search_result_finalize;
  object_class->get_property = search_result_get_property;
  object_class->set_property = search_result_set_property;

  properties[PROP_ID] =
      g_param_spec_string ("id",
                           "Id",
                           "Identifier of the search result",
                           NULL /* default value */,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_NAME] =
      g_param_spec_string ("name",
                           "Name",
                           "Display name of the search result",
                           NULL /* default value */,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_ICON] =
      g_param_spec_object ("icon",
                           "Icon",
                           "Icon of the search result, as a GIcon",
                           G_TYPE_ICON,
                           G_PARAM_READWRITE);
  properties[PROP_ICON_DATA] =
      g_param_spec_boxed ("icon-data",
                          "Icon data",
                          "Icon of the search result, as raw data",
                          SEARCH_TYPE_RESULT_ICON_DATA,
                          G_PARAM_READWRITE);
  properties[PROP_DESCRIPTION] =
      g_param_spec_string ("description",
                           "Description",
                           "An optional short description of the search result",
                           NULL /* default value */,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_CLIPBOARD_TEXT] =
      g_param_spec_string ("clipboard-text",
                           "Clipboard Text",
                           "Optional text to copy to clipboard when result is activated",
                           NULL /* default value */,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_PROVIDER_NAME] =
      g_param_spec_string ("provider-name",
                           "Provider name",
                           "Name of the search provider that produced this result",
                           NULL /* default value */,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
search_result_init (SearchResult *self)
{
}

/**
 * search_result_get_id:
 * @self: A #SearchResult
 *
 * Get the provider-specific ID of the search result.
 *
 * Returns: (transfer none): a string
 */
const gchar *
search_result_get_id (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->id;
}

/**
 * search_result_get_name:
 * @self: A #SearchResult
 *
 * Get the display name of the search result.
 *
 * Returns: (transfer none): a string
 */
const gchar *
search_result_get_name (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->name;
}

/**
 * search_result_get_icon:
 * @self: A #SearchResult
 *
 * Get the icon of the search result as a #GIcon, if available.
 *
 * The icon may instead be available as raw data, accessed via
 * [method@SearchResult.get_icon_data].
 *
 * Returns: (transfer none): a #GIcon, or %NULL
 */
GIcon *
search_result_get_icon (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->icon;
}

/**
 * search_result_get_icon_data:
 * @self: A #SearchResult
 *
 * Get the icon of the search result as raw data, if available.
 *
 * The icon may instead be available as a #GIcon, accessed via
 * [method@SearchResult.get_icon].
 *
 * Returns: (transfer none): a #SearchResultIconData, or %NULL
 */
SearchResultIconData *
search_result_get_icon_data (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->icon_data;
}

/**
 * search_result_get_description:
 * @self: A #SearchResult
 *
 * Get the optional short description of the search result, if any.
 *
 * Returns: (transfer none): a string, or %NULL
 */
const gchar *
search_result_get_description (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->description;
}

/**
 * search_result_get_clipboard_text:
 * @self: A #SearchResult
 *
 * Get the optional text to copy to clipboard when the result is activated.
 *
 * Returns: (transfer none): a string, or %NULL
 */
const gchar *
search_result_get_clipboard_text (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->clipboard_text;
}

/**
 * search_result_get_provider_name:
 * @self: A #SearchResult
 *
 * The name of the search provider that produced this result.
 *
 * Returns: (transfer none): a string
 */
const gchar *
search_result_get_provider_name (SearchResult *self)
{
  g_return_val_if_fail (SEARCH_IS_RESULT (self), NULL);

  return self->provider_name;
}
