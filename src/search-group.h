// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib-object.h>

#include "search-enums.h"
#include "search-provider.h"
#include "search-query.h"
#include "search-result-list.h"

G_BEGIN_DECLS

#define SEARCH_TYPE_GROUP (search_group_get_type ())

G_DECLARE_FINAL_TYPE (SearchGroup, search_group, SEARCH, GROUP, GObject)

SearchGroup *_search_group_new (GList *providers, SearchQuery *query, gboolean typeahead, guint typeahead_delay);

SearchQuery *search_group_get_query (SearchGroup *self);
SearchStatus search_group_get_status (SearchGroup *self);
gboolean search_group_get_typeahead (SearchGroup *self);
guint search_group_get_typeahead_delay (SearchGroup *self);

GList *search_group_get_providers (SearchGroup *self);
SearchProvider *search_group_get_provider (SearchGroup *self, const gchar *provider_id);

GList *search_group_get_errors (SearchGroup *self);
GError *search_group_get_error_for_provider (SearchGroup *self, const gchar *provider_id);

GList *search_group_get_result_lists (SearchGroup *self);
SearchResultList *search_group_get_result_list_for_provider (SearchGroup *self, const gchar *provider_id);

guint search_group_get_elapsed_time (SearchGroup *self);
guint search_group_get_elapsed_time_for_provider (SearchGroup *self, const gchar *provider_id);

void search_group_set_query (SearchGroup *self, SearchQuery *query);

void search_group_start (SearchGroup *self);
void search_group_force_stop (SearchGroup *self);

G_END_DECLS
