// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include <glib.h>

#include "search-error.h"

G_DEFINE_QUARK (search-error-quark, search_error)
