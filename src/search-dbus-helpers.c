/* Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "search-dbus-helpers.h"

/**
 * _search_dbus_get_org_gnome_shell_search_provider_dbus_proxy:
 * @conn: a #GDBusConnection, or %NULL to use session bus.
 * @version: D-Bus interface version, or 0 to use the latest version.
 *
 * Return a #GDBusProxy to a service that implements the
 * [org.gnome.Shell.SearchProvider](https://developer-old.gnome.org/shell/stable/gdbus-org.gnome.Shell.SearchProvider.html)
 * D-Bus interface.
 */
GDBusProxy *
_search_dbus_get_org_gnome_shell_search_provider_dbus_proxy (GDBusConnection *conn,
                                                             guint version,
                                                             const gchar *bus_name,
                                                             const gchar *object_path,
                                                             GError **error)
{
  const gchar *interface = NULL;

  g_return_val_if_fail (version <= 2, NULL);

  if (version == 1)
    {
      interface = "org.gnome.Shell.SearchProvider";
    }
  else
    {
      interface = "org.gnome.Shell.SearchProvider2";
    }

  if (conn)
    {
      return g_dbus_proxy_new_sync (conn, G_DBUS_PROXY_FLAGS_NONE, NULL, bus_name, object_path, interface, NULL, error);
    }
  else
    {
      return g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION, G_DBUS_PROXY_FLAGS_NONE, NULL, bus_name, object_path, interface, NULL, error);
    }
}

/**
 * _search_dbus_call_get_initial_result_set:
 * @proxy: A #GDBusProxy for the org.gnome.Shell.SearchProvider D-Bus interface
 * @search_text: string to pass as the only search term
 * @timeout: timeout in milliseconds to wait for results
 * @cancellable: A #GCancellable
 * @error: Return value for a #GError
 *
 * Call the [org.gnome.Shell.SearchProvider.GetInitialResultSet](https://developer-old.gnome.org/shell/stable/gdbus-org.gnome.Shell.SearchProvider.html#gdbus-method-org-gnome-Shell-SearchProvider.GetInitialResultSet)
 * method.
 *
 * The return value is an array of strings. Each string identifies one search
 * result, using an identifier that is specific to the search provider.
 *
 * Returns: (transfer full): a #GVariant with type `as`
 */
GVariant *
_search_dbus_call_get_initial_result_set (GDBusProxy *proxy,
                                          const gchar *search_text,
                                          gint timeout,
                                          GCancellable *cancellable,
                                          GError **error)
{
  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE_STRING_ARRAY);

  /* There can be multiple terms, for now we just pass one */
  g_variant_builder_add (&builder, "s", search_text);

  GVariant *params_array = g_variant_builder_end (&builder);
  GVariant *params = g_variant_new_tuple (&params_array, 1);

  g_autoptr (GVariant) results;

  results = g_dbus_proxy_call_sync (proxy, "GetInitialResultSet", params,
                                    G_DBUS_CALL_FLAGS_NONE, timeout, cancellable, error);

  if (results)
    {
      if (g_variant_is_of_type (results, G_VARIANT_TYPE ("(as)")))
        {
          return g_variant_get_child_value (results, 0);
        }

      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   "Invalid response from the service - incorrect return type for GetInitialResultSet.");
    }

  return NULL;
}

/**
 * _search_dbus_call_get_subsearch_result_set:
 * @proxy: A #GDBusProxy for the org.gnome.Shell.SearchProvider D-Bus interface
 * @search_text: string to pass as the only search term
 * @result_ids: previous results as a #GPtrArray
 * @timeout: timeout in milliseconds to wait for results
 * @cancellable: A #GCancellable
 * @error: Return value for a #GError
 *
 * Call the [org.gnome.Shell.SearchProvider.GetSubsearchResultSet](https://developer-old.gnome.org/shell/stable/gdbus-org.gnome.Shell.SearchProvider.html#gdbus-method-org-gnome-Shell-SearchProvider.GetSubsearchResultSet)
 * method.
 *
 * The return value is an array of strings. Each string identifies one search
 * result, using an identifier that is specific to the search provider.
 *
 * Returns: (transfer full): a #GVariant with type `as`
 */
GVariant *
_search_dbus_call_get_subsearch_result_set (GDBusProxy *proxy, const gchar *search_text, GPtrArray *result_ids, gint timeout, GCancellable *cancellable, GError **error)
{
  GVariantBuilder builder;
  g_variant_builder_init (&builder, G_VARIANT_TYPE_STRING_ARRAY);

  /* There can be multiple terms, for now we just pass one */
  g_variant_builder_add (&builder, "s", search_text);

  GVariant *result_ids_variant = g_variant_new_strv ((const gchar **) result_ids->pdata, result_ids->len);
  g_variant_builder_add_value (&builder, result_ids_variant);

  GVariant *params_array = g_variant_builder_end (&builder);
  GVariant *params = g_variant_new_tuple (&params_array, 1);

  g_autoptr (GVariant) results;

  results = g_dbus_proxy_call_sync (proxy, "GetSubsearchResultSet", params,
                                    G_DBUS_CALL_FLAGS_NONE, timeout, cancellable, error);

  if (results)
    {
      if (g_variant_is_of_type (results, G_VARIANT_TYPE ("(as)")))
        {
          return g_variant_get_child_value (results, 0);
        }

      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   "Invalid response from the service - incorrect return type for GetSubsearchResultSet.");
    }

  return NULL;
}

/**
 * _search_dbus_call_get_result_metas:
 * @proxy: A #GDBusProxy for the org.gnome.Shell.SearchProvider D-Bus interface
 * @result_ids: A #GPtrArray containing result identifier
 * @timeout: timeout in milliseconds to wait for results
 * @cancellable: A #GCancellable
 * @error: Return value for a #GError
 *
 * Call the [org.gnome.Shell.SearchProvider.GetResultMetas](https://developer-old.gnome.org/shell/stable/gdbus-org.gnome.Shell.SearchProvider.html#gdbus-method-org-gnome-Shell-SearchProvider.GetResultMetas)
 * method.
 *
 * Return value is a dictionary of key/value pairs. The supported keys are
 * documented [here](https://developer.gnome.org/documentation/tutorials/search-provider.html#getresultmetas-as-aa-sv).
 *
 * Returns: a #GVariant of type `aa{sv}`.
 */
GVariant *
_search_dbus_call_get_result_metas (GDBusProxy *proxy, GPtrArray *result_ids, gint timeout, GCancellable *cancellable, GError **error)
{
  g_return_val_if_fail (result_ids != NULL, NULL);

  GVariantBuilder args_builder;
  GVariant *args = NULL;
  g_autoptr (GVariant) metas;
  GVariant *metas_child = NULL;

  GVariant *result_ids_variant = g_variant_new_strv ((const gchar **) result_ids->pdata, result_ids->len);

  g_variant_builder_init (&args_builder, G_VARIANT_TYPE_TUPLE);
  g_variant_builder_add_value (&args_builder, result_ids_variant);
  args = g_variant_builder_end (&args_builder);

  /* This will take the floating reference of 'args' */
  metas = g_dbus_proxy_call_sync (proxy, "GetResultMetas", args, G_DBUS_CALL_FLAGS_NONE, timeout, cancellable, error);

  if (metas)
    {
      if (g_variant_is_of_type (metas, G_VARIANT_TYPE ("(aa{sv})")))
        {
          metas_child = g_variant_get_child_value (metas, 0);
          /* Serialize the child value to ensure borrowed values are valid. */
          g_variant_get_data (metas_child);

          return metas_child;
        }

      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                   "Invalid response from the service - incorrect return type for GetResultMetas.");
    };

  return NULL;
}
