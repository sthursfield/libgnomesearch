// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include "search-context.h"
#include "search-group.h"
#include "search-provider-dbus.h"
#include "search-provider.h"
#include "search-result-list.h"

/**
 * SearchContext:
 *
 * This class provides the entry point for interacting with the libgnomesearch library.
 */

/* Typeahead delay constants.
 *
 * We allow setting the delay to zero because the app may implement its own
 * debouncing feature. For example GtkSearchEntry does so.
 *
 * Values in milliseconds.
 */
#define MIN_TYPEAHEAD_DELAY 0
#define MAX_TYPEAHEAD_DELAY 10000
#define DEFAULT_TYPEAHEAD_DELAY 250

/* Default location to look for search providers. */
#define DEFAULT_SEARCH_PROVIDER_DBUS_DIR "/usr/share/gnome-shell/search-providers/"

struct _SearchContext
{
  GObject parent_instance;

  guint typeahead_delay;

  gchar *search_provider_dir;
  GDBusConnection *dbus_connection; /* NULL to use session bus */

  GList *loaded_providers;
};

enum
{
  PROP_0,
  PROP_TYPEAHEAD_DELAY,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

G_DEFINE_FINAL_TYPE (SearchContext, search_context, G_TYPE_OBJECT)

/**
 * search_context_new:
 *
 * Returns: a newly created #SearchContext instance
 */
SearchContext *
search_context_new (void)
{
  return g_object_new (SEARCH_TYPE_CONTEXT, NULL);
}

static void
search_context_finalize (GObject *object)
{
  SearchContext *self = (SearchContext *) object;

  g_clear_object (&self->dbus_connection);

  g_list_free_full (self->loaded_providers, g_object_unref);
  self->loaded_providers = NULL;

  g_clear_pointer (&self->search_provider_dir, g_free);

  G_OBJECT_CLASS (search_context_parent_class)->finalize (object);
}

static void
search_context_get_property (GObject *object,
                             guint prop_id,
                             GValue *value,
                             GParamSpec *pspec)
{
  SearchContext *self = SEARCH_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_TYPEAHEAD_DELAY:
      g_value_set_uint (value, self->typeahead_delay);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_context_set_property (GObject *object,
                             guint prop_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
  SearchContext *self = SEARCH_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_TYPEAHEAD_DELAY:
      self->typeahead_delay = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_context_class_init (SearchContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = search_context_finalize;
  object_class->get_property = search_context_get_property;
  object_class->set_property = search_context_set_property;

  properties[PROP_TYPEAHEAD_DELAY] =
      g_param_spec_uint ("typeahead-delay",
                         "Typeahead delay",
                         "Delay before running a search, when typeahead is enabled",
                         MIN_TYPEAHEAD_DELAY,
                         MAX_TYPEAHEAD_DELAY,
                         DEFAULT_TYPEAHEAD_DELAY,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
search_context_init (SearchContext *self)
{
  self->dbus_connection = NULL; /* Default to session bus for D-Bus search providers. */
  self->search_provider_dir = g_strdup (DEFAULT_SEARCH_PROVIDER_DBUS_DIR);

  self->typeahead_delay = DEFAULT_TYPEAHEAD_DELAY;

  self->loaded_providers = NULL;
}

/* Load DBus search providers from the configured search provider locations. */
static GList *
load_dbus_search_providers (SearchContext *self, gboolean typeahead, GError **error)
{
  GList *providers = NULL;
  GDir *dir = NULL;
  GError *inner_error = NULL;

  dir = g_dir_open (self->search_provider_dir, 0, &inner_error);
  if (inner_error)
    {
      g_propagate_error (error, inner_error);
      return NULL;
    }

  const gchar *filename = NULL;
  while ((filename = g_dir_read_name (dir)) != NULL)
    {
      gchar *filepath = g_build_filename (self->search_provider_dir, filename, NULL);

      SearchProviderDBus *provider = search_provider_dbus_new_from_file (self->dbus_connection, filepath, typeahead, self->typeahead_delay, &inner_error);

      if (inner_error)
        {
          g_warning ("Failed to load search provider %s: %s", filepath, inner_error->message);
          g_clear_error (&inner_error);
        }
      else
        {
          providers = g_list_append (providers, provider);
        }

      g_free (filepath);
    }

  g_dir_close (dir);
  return providers;
}

/**
 * search_context_set_dbus_connection:
 * @self: a #SearchContext
 * @conn: (allow-none): a #GDBusConnection, or %NULL to use the session bus.
 *
 * Set the DBus connection where DBus search providers will be activated.
 *
 * This will not affect existing search providers. New search providers
 * created with [method@SearchContext.load_providers] will use the provided
 * connection.
 */
void
search_context_set_dbus_connection (SearchContext *self, GDBusConnection *conn)
{
  g_set_object (&self->dbus_connection, conn);
}

/**
 * search_context_set_search_provider_dir:
 * @dir: a string
 *
 * Set the location where D-Bus search providers are configured.
 */
void
search_context_set_search_provider_dir (SearchContext *self, const gchar *dir)
{
  g_return_if_fail (SEARCH_IS_CONTEXT (self));
  g_return_if_fail (dir != NULL);

  g_free (self->search_provider_dir);
  self->search_provider_dir = g_strdup (dir);
}

/**
 * search_context_load_providers:
 * @self: a #SearchContext
 * @typeahead: %TRUE to load providers in typeahead mode, %FALSE for single-shot search mode.
 *
 * Instantiate all supported search providers and return them as a list.
 *
 * The providers are owned by the context and will be destroyed if
 * [method@SearchContext.load_providers] is called again, unless you ref
 * them yourself.
 *
 * Returns: (transfer none) (element-type SearchProvider): a
 * #GList of #SearchProvider instances
 */
GList *
search_context_load_providers (SearchContext *self, gboolean typeahead)
{
  g_return_val_if_fail (SEARCH_IS_CONTEXT (self), NULL);
  g_autoptr (GError) error = NULL;

  g_list_free_full (self->loaded_providers, g_object_unref);
  self->loaded_providers = NULL;

  self->loaded_providers = load_dbus_search_providers (self, typeahead, &error);
  if (error)
    {
      g_warning ("Failed to reload DBus search providers: %s", error->message);
    }

  return self->loaded_providers;
}

/**
 * search_context_create_oneshot_search:
 * @self: a #SearchContext
 * @query: (transfer none): a #SearchQuery
 *
 * Create a oneshot search operation. The search query cannot be changed
 * once the search has started.
 *
 * Use [method@SearchGroup.start()] to start
 * searching once you have connected appropriate signal handlers.
 *
 * Returns: (transfer full): a #SearchGroup
 */
SearchGroup *
search_context_create_oneshot_search (SearchContext *self,
                                      SearchQuery *query)
{
  GList *providers;
  SearchGroup *group = NULL;

  providers = search_context_load_providers (self, FALSE);
  g_list_foreach (providers, (GFunc) search_provider_set_query, query);
  group = _search_group_new (providers, query, FALSE, 0);

  return group;
}

/**
 * search_context_create_typeahead_search:
 * @self: a #SearchContext
 * @query: (transfer none): a #SearchQuery
 *
 * Create a typeahead search operation. This type of search allows changing
 * the query while the search is running, with a delay based on the
 * [property@SearchContext:typeahead-delay] property.
 *
 * Use [method@SearchGroup.start()] to start searching once you have connected appropriate signal handlers.
 *
 * Returns: (transfer full): a #SearchGroup
 */
SearchGroup *
search_context_create_typeahead_search (SearchContext *self,
                                        SearchQuery *query)
{
  GList *providers;
  SearchGroup *group = NULL;

  providers = search_context_load_providers (self, TRUE);
  g_list_foreach (providers, (GFunc) search_provider_set_query, query);
  group = _search_group_new (providers, query, TRUE, self->typeahead_delay);

  return group;
}
