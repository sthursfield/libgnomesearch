// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <gio/gio.h>
#include <glib-object.h>

#include "search-result-icon-data.h"

G_BEGIN_DECLS

#define SEARCH_TYPE_RESULT (search_result_get_type ())

G_DECLARE_FINAL_TYPE (SearchResult, search_result, SEARCH, RESULT, GObject)

SearchResult *search_result_new (const gchar *id,
                                 const gchar *name,
                                 GIcon *icon,
                                 SearchResultIconData *icon_data,
                                 const gchar *description,
                                 const gchar *clipboard_text,
                                 const gchar *provider_name);

const gchar *search_result_get_id (SearchResult *self);
const gchar *search_result_get_name (SearchResult *self);
GIcon *search_result_get_icon (SearchResult *self);
SearchResultIconData *search_result_get_icon_data (SearchResult *self);
const gchar *search_result_get_description (SearchResult *self);
const gchar *search_result_get_clipboard_text (SearchResult *self);
const gchar *search_result_get_provider_name (SearchResult *self);

G_END_DECLS
