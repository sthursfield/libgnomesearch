/* Copyright 2023 Kaspar Matas
 * Copyright 2023 Sam Thursfield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

G_GNUC_INTERNAL
GDBusProxy *_search_dbus_get_org_gnome_shell_search_provider_dbus_proxy (GDBusConnection *conn, guint version, const gchar *bus_name, const gchar *object_path, GError **error);

G_GNUC_INTERNAL
GVariant *_search_dbus_call_get_initial_result_set (GDBusProxy *proxy, const gchar *search_text, gint timeout, GCancellable *cancellable, GError **error);

G_GNUC_INTERNAL
GVariant *_search_dbus_call_get_subsearch_result_set (GDBusProxy *proxy, const gchar *search_text, GPtrArray *result_ids, gint timeout, GCancellable *cancellable, GError **error);

G_GNUC_INTERNAL
GVariant *_search_dbus_call_get_result_metas (GDBusProxy *proxy, GPtrArray *result_ids, gint timeout, GCancellable *cancellable, GError **error);

G_END_DECLS
