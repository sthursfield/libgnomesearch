// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include "search-provider.h"
#include "enum-types.h"

#include "search-query.h"

/* Typeahead delay constants.
 *
 * We allow setting the delay to zero because the app may implement its own
 * debouncing feature. For example GtkSearchEntry does so.
 *
 * Values in milliseconds.
 */
#define MIN_TYPEAHEAD_DELAY 0
#define MAX_TYPEAHEAD_DELAY 10000
#define DEFAULT_TYPEAHEAD_DELAY 250

G_DEFINE_INTERFACE (SearchProvider, search_provider, G_TYPE_OBJECT)

static void
search_provider_default_init (SearchProviderInterface *iface)
{
  g_object_interface_install_property (iface,
                                       g_param_spec_string ("id",
                                                            "Id",
                                                            "Search provider internal ID",
                                                            "missing-id",
                                                            G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_interface_install_property (iface,
                                       g_param_spec_string ("name",
                                                            "Name",
                                                            "Search provider display name",
                                                            "unnamed provider",
                                                            G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_interface_install_property (iface,
                                       g_param_spec_boxed ("query",
                                                           "Query",
                                                           "Query to search for",
                                                           SEARCH_TYPE_QUERY,
                                                           G_PARAM_READWRITE));
  g_object_interface_install_property (iface,
                                       g_param_spec_object ("result-list",
                                                            "Result list",
                                                            "Result list object",
                                                            SEARCH_TYPE_RESULT_LIST,
                                                            G_PARAM_READABLE));
  g_object_interface_install_property (iface,
                                       g_param_spec_boolean ("running",
                                                             "Running",
                                                             "True while search provider is running the search",
                                                             FALSE,
                                                             G_PARAM_READABLE));
  g_object_interface_install_property (iface,
                                       g_param_spec_boolean ("typeahead",
                                                             "Typeahead",
                                                             "True if typeahead search is enabled for this provider",
                                                             FALSE,
                                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_interface_install_property (iface,
                                       g_param_spec_uint ("typeahead-delay",
                                                          "Typeahead delay",
                                                          "Delay in milliseconds before responding to changes in the search query",
                                                          MIN_TYPEAHEAD_DELAY,
                                                          MAX_TYPEAHEAD_DELAY,
                                                          DEFAULT_TYPEAHEAD_DELAY,
                                                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));

  g_signal_new ("started",
                SEARCH_TYPE_PROVIDER,
                G_SIGNAL_RUN_FIRST,
                0,    /* class offset */
                NULL, /* accumulator */
                NULL, /* accumulator data */
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE,
                0);

  g_signal_new ("stopped",
                SEARCH_TYPE_PROVIDER,
                G_SIGNAL_RUN_FIRST,
                0,    /* class offset */
                NULL, /* accumulator */
                NULL, /* accumulator data */
                g_cclosure_marshal_generic,
                G_TYPE_NONE,
                3,
                SEARCH_TYPE_STOP_REASON,
                G_TYPE_DOUBLE,
                G_TYPE_ERROR);
}

/**
 * search_provider_get_id:
 * @self: a #SearchProvider
 *
 * Get the internal identifier of this search provider.
 *
 * Returns: a string, owned by the caller
 */
gchar *
search_provider_get_id (SearchProvider *self)
{
  gchar *id;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), NULL);

  g_object_get (self, "id", &id, NULL);

  return id;
}

/**
 * search_provider_get_name:
 * @self: a #SearchProvider
 *
 * Get the display name of this search provider.
 *
 * Returns: a string, owned by the caller
 */
gchar *
search_provider_get_name (SearchProvider *self)
{
  gchar *name;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), NULL);

  g_object_get (self, "name", &name, NULL);

  return name;
}

/**
 * search_provider_get_result_list:
 * @self: a #SearchProvider
 *
 * Get the result list of this search provider.
 *
 * Returns: (transfer full): a #SearchResultList, owned by the caller
 */
SearchResultList *
search_provider_get_result_list (SearchProvider *self)
{
  SearchResultList *result_list;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), NULL);

  g_object_get (self, "result-list", &result_list, NULL);

  return result_list;
}

/**
 * search_provider_get_running:
 * @self: a #SearchProvider
 *
 * Get the running status of this search provider.
 *
 * Returns: %TRUE if provider is currently searching, %FALSE otherwise
 */
gboolean
search_provider_get_running (SearchProvider *self)
{
  gboolean running;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), FALSE);

  g_object_get (self, "running", &running, NULL);

  return running;
}

/**
 * search_provider_get_typeahead:
 * @self: a #SearchProvider
 *
 * Get the typeahead search state of this search provider.
 *
 * Returns: %TRUE if provider has typeahead search enabled, %FALSE otherwise
 */
gboolean
search_provider_get_typeahead (SearchProvider *self)
{
  gboolean typeahead;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), FALSE);

  g_object_get (self, "typeahead", &typeahead, NULL);

  return typeahead;
}

/**
 * search_provider_get_typeahead_delay:
 * @self: a #SearchProvider
 *
 * Get the typeahead search delay of this search provider.
 *
 * Returns: a #guint value in milliseconds.
 */
guint
search_provider_get_typeahead_delay (SearchProvider *self)
{
  guint typeahead_delay;

  g_return_val_if_fail (SEARCH_IS_PROVIDER (self), 0);

  g_object_get (self, "typeahead-delay", &typeahead_delay, NULL);

  return typeahead_delay;
}

/**
 * search_provider_set_query:
 * @self: a #SearchProvider
 * @query: (transfer none):a #SearchQuery
 *
 * Set the search query.
 *
 * If [property@SearchProvider:typeahead] mode is enabled, the search query can be
 * set at any time. Otherwise, it can only be modified while the provider is
 * not running a search.
 */
void
search_provider_set_query (SearchProvider *self, SearchQuery *query)
{
  g_return_if_fail (SEARCH_IS_PROVIDER (self));

  g_object_set (self, "query", query, NULL);
}

/**
 * search_provider_start:
 * @self: a #SearchProvider
 *
 * Start searching for results.
 *
 * The [signal@SearchProvider::started] signal will be emitted whenever the provider
 * starts working. It is emitted exactly once for one-shot searches, and one or more
 * times for typeahead searches.
 *
 * The [signal@SearchProvider::stopped] signal is emitted when the provider
 * finishes searching, hits an error, or is explicitly stopped. For typeahead
 * searches, this may be emitted multiple times as the query changes.
 *
 * This function can only be called once on a search provider.
 */
void
search_provider_start (SearchProvider *self)
{
  SearchProviderInterface *iface;

  g_return_if_fail (SEARCH_IS_PROVIDER (self));

  iface = SEARCH_PROVIDER_GET_IFACE (self);

  g_return_if_fail (iface->start != NULL);
  iface->start (self);
}

/**
 * search_provider_force_stop:
 * @self: a #SearchProvider
 *
 * Forcibly stop searching for results.
 *
 * The [signal@SearchProvider::stopped] signal will be emitted when the provider actually stops
 * work.
 */
void
search_provider_force_stop (SearchProvider *self)
{
  SearchProviderInterface *iface;

  g_return_if_fail (SEARCH_IS_PROVIDER (self));

  iface = SEARCH_PROVIDER_GET_IFACE (self);

  g_return_if_fail (iface->force_stop != NULL);
  iface->force_stop (self);
}
