// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib-object.h>

#include "search-result.h"

G_BEGIN_DECLS

#define SEARCH_TYPE_RESULT_LIST (search_result_list_get_type ())

G_DECLARE_FINAL_TYPE (SearchResultList, search_result_list, SEARCH, RESULT_LIST, GObject)

SearchResultList *search_result_list_new (void);

void search_result_list_append (SearchResultList *self, SearchResult *item);
void search_result_list_insert (SearchResultList *self, guint position, SearchResult *item);
void search_result_list_remove (SearchResultList *self, guint position);
void search_result_list_remove_all (SearchResultList *self);

G_END_DECLS
