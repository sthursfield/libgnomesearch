// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <gio/gio.h>
#include <glib-object.h>

#include "search-group.h"
#include "search-query.h"

G_BEGIN_DECLS

#define SEARCH_TYPE_CONTEXT (search_context_get_type ())

G_DECLARE_FINAL_TYPE (SearchContext, search_context, SEARCH, CONTEXT, GObject)

SearchContext *search_context_new (void);

void search_context_set_dbus_connection (SearchContext *self, GDBusConnection *conn);
void search_context_set_search_provider_dir (SearchContext *self, const gchar *dir);

GList *search_context_load_providers (SearchContext *self, gboolean typeahead);

SearchGroup *search_context_create_oneshot_search (SearchContext *self,
                                                   SearchQuery *query);
SearchGroup *search_context_create_typeahead_search (SearchContext *self,
                                                     SearchQuery *query);

G_END_DECLS
