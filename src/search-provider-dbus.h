/* Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define SEARCH_TYPE_PROVIDER_DBUS (search_provider_dbus_get_type ())

G_DECLARE_FINAL_TYPE (SearchProviderDBus,
                      search_provider_dbus,
                      SEARCH,
                      PROVIDER_DBUS,
                      GObject)

SearchProviderDBus *search_provider_dbus_new_from_file (GDBusConnection *dbus_connection, const gchar *path, gboolean typeahead, guint typeahead_delay, GError **error);

const gchar *search_provider_dbus_get_bus_name (
    SearchProviderDBus *provider);

const gchar *search_provider_dbus_get_object_path (
    SearchProviderDBus *provider);

const gchar *search_provider_dbus_get_desktop_id (
    SearchProviderDBus *provider);

guint search_provider_dbus_get_version (
    SearchProviderDBus *provider);

guint search_provider_dbus_get_timeout_msec (
    SearchProviderDBus *provider);

G_END_DECLS
