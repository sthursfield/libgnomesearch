/* Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib.h>

#include "search-provider-dbus.h"

#include "enum-types.h"
#include "search-dbus-helpers.h"
#include "search-dbus-worker.h"
#include "search-provider.h"
#include "search-query.h"
#include "search-result.h"

/**
 * SearchProviderDBus:
 *
 * A search provider that fetches results from a specific app or service on
 * the D-Bus session bus, using the
 * [org.gnome.Shell.SearchProvider](https://developer-old.gnome.org/shell/stable/gdbus-org.gnome.Shell.SearchProvider.html)
 * D-Bus interface.
 */

/* Typeahead delay constants.
 *
 * We allow setting the delay to zero because the app may implement its own
 * debouncing feature. For example GtkSearchEntry does so.
 *
 * Values in milliseconds.
 */
#define MIN_TYPEAHEAD_DELAY 0
#define MAX_TYPEAHEAD_DELAY 10000
#define DEFAULT_TYPEAHEAD_DELAY 250

#define SEARCH_PROVIDERS_SETTINGS_SCHEMA \
  "org.gnome.desktop.search-providers"

struct _SearchProviderDBus
{
  GObject parent_instance;

  gchar *id;
  gchar *name;
  SearchQuery *query;
  SearchResultList *result_list;
  gboolean running;
  gboolean typeahead;
  guint typeahead_delay;

  GDBusConnection *dbus_connection;
  gchar *bus_name;
  gchar *object_path;
  gchar *desktop_id;
  guint version;
  guint timeout_msec;

  GMainContext *main_context;
  GThread *worker;
  GAsyncQueue *worker_rx; /* Queue of _SearchDBusWorkerMessage */
  GAsyncQueue *worker_tx; /* Queue of _SearchDBusWorkerMessage */
  GCancellable *cancellable;

  GDBusProxy *proxy;
};

enum
{
  PROP_0 = 0,
  PROP_ID = 1,
  PROP_NAME = 2,
  PROP_QUERY = 3,
  PROP_RESULT_LIST = 4,
  PROP_RUNNING = 5,
  PROP_TYPEAHEAD = 6,
  PROP_TYPEAHEAD_DELAY = 7,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static void search_provider_iface_init (SearchProviderInterface *iface);

G_DEFINE_TYPE_WITH_CODE (SearchProviderDBus,
                         search_provider_dbus,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEARCH_TYPE_PROVIDER,
                                                search_provider_iface_init))
static void
send_set_query (SearchProviderDBus *self)
{
  SearchDBusWorkerMessage *message;

  message = g_new0 (SearchDBusWorkerMessage, 1);
  message->message_type = SEARCH_DBUS_WORKER_MESSAGE_SET_QUERY;
  message->set_query_data.query = search_query_copy (self->query);

  g_async_queue_push (self->worker_rx, message);
}

static void
search_provider_dbus_get_property (GObject *object,
                                   guint prop_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    case PROP_QUERY:
      g_value_set_boxed (value, self->query);
      break;
    case PROP_RESULT_LIST:
      g_value_set_object (value, self->result_list);
      break;
    case PROP_RUNNING:
      g_value_set_boolean (value, self->running);
      break;
    case PROP_TYPEAHEAD:
      g_value_set_boolean (value, self->typeahead);
      break;
    case PROP_TYPEAHEAD_DELAY:
      g_value_set_uint (value, self->typeahead_delay);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_provider_dbus_set_property (GObject *object,
                                   guint prop_id,
                                   const GValue *value,
                                   GParamSpec *pspec)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_assert (self->id == FALSE);
      self->id = g_value_dup_string (value);
      break;
    case PROP_NAME:
      g_assert (self->name == FALSE);
      self->name = g_value_dup_string (value);
      break;
    case PROP_QUERY:
      self->query = search_query_copy (g_value_get_boxed (value));

      if (self->running)
        {
          send_set_query (self);
        }

      break;
    case PROP_RESULT_LIST:
      g_assert (self->result_list == NULL);
      self->result_list = g_value_get_object (value);
      break;
    case PROP_RUNNING:
      g_assert_not_reached ();
      break;
    case PROP_TYPEAHEAD:
      self->typeahead = g_value_get_boolean (value);
      break;
    case PROP_TYPEAHEAD_DELAY:
      self->typeahead_delay = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_provider_dbus_dispose (GObject *object)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (object);

  if (self->worker != NULL)
    {
      search_provider_force_stop (SEARCH_PROVIDER (self));

      g_thread_join (self->worker);
      self->worker = NULL;
    }

  if (self->proxy != NULL)
    {
      g_object_run_dispose (G_OBJECT (self->proxy));
    }

  G_OBJECT_CLASS (search_provider_dbus_parent_class)->dispose (object);
}

static void
search_provider_dbus_finalize (GObject *object)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (object);

  g_clear_object (&self->proxy);

  g_clear_pointer (&self->worker_rx, g_async_queue_unref);
  g_clear_pointer (&self->worker_tx, g_async_queue_unref);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->main_context, g_main_context_unref);

  g_clear_pointer (&self->query, search_query_free);
  g_clear_object (&self->result_list);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->bus_name, g_free);
  g_clear_pointer (&self->object_path, g_free);
  g_clear_pointer (&self->desktop_id, g_free);
  g_clear_object (&self->dbus_connection);

  G_OBJECT_CLASS (search_provider_dbus_parent_class)->finalize (object);
}

static void
search_provider_dbus_class_init (
    SearchProviderDBusClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = search_provider_dbus_get_property;
  object_class->set_property = search_provider_dbus_set_property;
  object_class->dispose = search_provider_dbus_dispose;
  object_class->finalize = search_provider_dbus_finalize;

  properties[PROP_ID] =
      g_param_spec_string ("id",
                           "Id",
                           "Search provider internal id",
                           "missing-dbus-provider-id",
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_NAME] =
      g_param_spec_string ("name",
                           "Name",
                           "Search provider display name",
                           "Generic D-Bus search provider",
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_QUERY] =
      g_param_spec_boxed ("query",
                          "Query",
                          "Query to search for",
                          SEARCH_TYPE_QUERY,
                          G_PARAM_READWRITE);
  properties[PROP_RESULT_LIST] =
      g_param_spec_object ("result-list",
                           "Result list",
                           "Result list",
                           SEARCH_TYPE_RESULT_LIST,
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  properties[PROP_RUNNING] =
      g_param_spec_boolean ("running",
                            "Running",
                            "True while search provider is running the search",
                            FALSE,
                            G_PARAM_READABLE);
  properties[PROP_TYPEAHEAD] =
      g_param_spec_boolean ("typeahead",
                            "Typeahead",
                            "Allow changing search terms after search starts",
                            FALSE,
                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  properties[PROP_TYPEAHEAD_DELAY] =
      g_param_spec_uint ("typeahead-delay",
                         "Typeahead delay",
                         "Delay before running a search, when typeahead is enabled",
                         MIN_TYPEAHEAD_DELAY,
                         MAX_TYPEAHEAD_DELAY,
                         DEFAULT_TYPEAHEAD_DELAY,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_override_property (object_class, PROP_ID, "id");
  g_object_class_override_property (object_class, PROP_NAME, "name");
  g_object_class_override_property (object_class, PROP_QUERY, "query");
  g_object_class_override_property (object_class, PROP_RESULT_LIST, "result-list");
  g_object_class_override_property (object_class, PROP_RUNNING, "running");
  g_object_class_override_property (object_class, PROP_TYPEAHEAD, "typeahead");
  g_object_class_override_property (object_class, PROP_TYPEAHEAD_DELAY, "typeahead-delay");
}

static void
search_provider_dbus_init (SearchProviderDBus *self)
{
  self->query = NULL;
  self->result_list = search_result_list_new ();

  self->main_context = g_main_context_ref_thread_default ();
  self->worker = NULL;
  self->worker_rx = g_async_queue_new ();
  self->worker_tx = g_async_queue_new ();
  self->cancellable = g_cancellable_new ();

  // Initialization of failable things such as the D-Bus proxy is deferred to
  // the start() vfunc.
}

/**
 * search_provider_dbus_new_from_file:
 * @path: a file path
 * @dbus_connection: a #GDBusConnection
 * @error: (allow-none): return location for a #GError
 *
 * Creates a new #SearchProviderDBus from a keyfile at @filepath.
 *
 * The format of the keyfile is documented at <https://developer-old.gnome.org/SearchProvider/>.
 *
 * Returns: a newly allocated #SearchProviderDBus, or %NULL on error.
 */
SearchProviderDBus *
search_provider_dbus_new_from_file (GDBusConnection *dbus_connection, const gchar *path, gboolean typeahead, guint typeahead_delay, GError **error)
{
  SearchProviderDBus *provider;
  g_autofree GError *inner_error = NULL;
  g_autoptr (GKeyFile) keyfile = g_key_file_new ();

  if (g_key_file_load_from_file (keyfile, path, G_KEY_FILE_NONE, &inner_error))
    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;
      gchar *bus_name;
      gchar *object_path;
      gchar *desktop_id;
      guint version;

      bus_name = g_key_file_get_string (keyfile, "Shell Search Provider", "BusName", &inner_error);
      if (inner_error)
        {
          g_propagate_error (error, inner_error);
          return NULL;
        }

      object_path = g_key_file_get_string (keyfile, "Shell Search Provider", "ObjectPath", &inner_error);
      if (inner_error)
        {
          g_propagate_error (error, inner_error);
          return NULL;
        }

      desktop_id = g_key_file_get_string (keyfile, "Shell Search Provider", "DesktopId", &inner_error);
      if (inner_error)
        {
          g_propagate_error (error, inner_error);
          return NULL;
        }

      version = g_key_file_get_integer (keyfile, "Shell Search Provider", "Version", &inner_error);
      if (inner_error)
        {
          g_propagate_error (error, inner_error);
          return NULL;
        }

      id = g_strdup_printf ("SearchDBusProvider(%s)", desktop_id);

      /* FIXME: make the name more human-readable */
      name = g_strdup_printf ("SearchDBusProvider(%s)", desktop_id);

      provider = g_object_new (SEARCH_TYPE_PROVIDER_DBUS,
                               "id", id,
                               "name", name,
                               "typeahead", typeahead,
                               "typeahead-delay", typeahead_delay,
                               NULL);

      if (dbus_connection != NULL)
        {
          provider->dbus_connection = g_object_ref (dbus_connection);
        }
      provider->bus_name = bus_name;
      provider->object_path = object_path;
      provider->desktop_id = desktop_id;
      provider->version = version;
      provider->timeout_msec = 2000;
    }
  else
    {
      g_propagate_error (error, inner_error);
      return NULL;
    }

  return provider;
}

/**
 * search_provider_dbus_get_bus_name:
 * @provider: a SearchProviderDBus
 *
 * Returns the *bus name* associated with this remote search provider.
 *
 * See the [D-Bus tutorial](https://dbus.freedesktop.org/doc/dbus-tutorial.html)
 * for more info about bus names.
 *
 * Returns: a string, owned by the search provider.
 */
const gchar *
search_provider_dbus_get_bus_name (
    SearchProviderDBus *provider)
{
  return provider->bus_name;
}

/**
 * search_provider_dbus_get_object_path:
 * @provider: a SearchProviderDBus
 *
 * Returns the *object path* associated with this remote search provider.
 *
 * See the [D-Bus tutorial](https://dbus.freedesktop.org/doc/dbus-tutorial.html)
 * for more info about D-Bus object paths.
 *
 * Returns: a string, owned by the search provider.
 */
const gchar *
search_provider_dbus_get_object_path (SearchProviderDBus *provider)
{
  return provider->object_path;
}

/**
 * search_provider_dbus_get_desktop_id:
 * @provider: a SearchProviderDBus
 *
 * Returns the *Desktop File ID* associated with this remote search provider.
 *
 * See the [XDG Desktop Entry Specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#desktop-file-id)
 * for more info about `.desktop` files.
 *
 * Returns: a string, owned by the search provider.
 */
const gchar *
search_provider_dbus_get_desktop_id (SearchProviderDBus *provider)
{
  return provider->desktop_id;
}

/**
 * search_provider_dbus_get_version:
 * @provider: a SearchProviderDBus
 *
 * Returns the version of the SearchProvider interface
 * that this search provider declares support for.
 *
 * See the [SearchProvider interface docs](https://developer-old.gnome.org/SearchProvider/)
 * for more info about this.
 *
 * Returns: a #guint, owned by the search provider.
 */
guint
search_provider_dbus_get_version (SearchProviderDBus *provider)
{
  return provider->version;
}

/**
 * search_provider_dbus_get_timeout_msec:
 * @provider: a SearchProviderDBus
 *
 * The search provider should return results within this timeout.
 *
 * Returns: a #guint, owned by the search provider.
 */
guint
search_provider_dbus_get_timeout_msec (SearchProviderDBus *provider)
{
  return provider->timeout_msec;
}

static void
send_stop (GAsyncQueue *worker_rx)
{
  SearchDBusWorkerMessage *message = g_new0 (SearchDBusWorkerMessage, 1);
  message->message_type = SEARCH_DBUS_WORKER_MESSAGE_STOP;
  g_async_queue_push (worker_rx, message);
}

static void
on_worker_message_ready (gpointer user_data)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (user_data);
  SearchDBusWorkerMessage *message;

  g_assert (g_main_context_is_owner (self->main_context));

  while ((message = g_async_queue_try_pop (self->worker_tx)))
    {
      g_debug ("%s: received '%s' from worker", self->id, _search_dbus_worker_message_name[message->message_type]);

      switch (message->message_type)
        {
        case SEARCH_DBUS_WORKER_MESSAGE_STARTED:
          g_assert (!self->running);

          self->running = TRUE;
          g_object_notify (G_OBJECT (self), "running");

          g_signal_emit_by_name (self, "started");

          break;

        case SEARCH_DBUS_WORKER_MESSAGE_STOPPED:
          if (self->running)
            {
              self->running = FALSE;
              g_object_notify (G_OBJECT (self), "running");
            }

          g_signal_emit_by_name (self, "stopped", message->stopped_data.stop_reason, message->stopped_data.elapsed_time, message->stopped_data.error);

          if (self->typeahead == FALSE)
            {
              SearchStopReason reason = message->stopped_data.stop_reason;

              if (reason == SEARCH_STOP_REASON_NO_RESULTS || reason == SEARCH_STOP_REASON_REACHED_RESULT_LIMIT)
                {
                  /* The worker is waiting for query changes, since we are not in 'typeahead' mode
                   * there will not be any query updates so signal it to exit already.
                   */
                  send_stop (self->worker_rx);
                }
            }

          break;

        case SEARCH_DBUS_WORKER_MESSAGE_SET_QUERY:
        case SEARCH_DBUS_WORKER_MESSAGE_STOP:
        default:
          g_assert_not_reached ();
          break;
        }

      g_free (message);
    }
}

static void
start (SearchProvider *provider)
{
  GError *error = NULL;
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (provider);

  g_return_if_fail (SEARCH_IS_PROVIDER_DBUS (self));
  g_return_if_fail (self->query != NULL);

  if (self->worker == NULL)
    {
      SearchDBusWorkerConfig *worker_config;

      worker_config = _search_dbus_worker_config_new (self->dbus_connection,
                                                     self->version,
                                                     self->bus_name,
                                                     self->object_path,
                                                     self->timeout_msec,
                                                     search_provider_get_name (SEARCH_PROVIDER (self)));
      self->worker = _search_dbus_worker_thread_start (self->main_context,
                                                       self->worker_rx,
                                                       self->worker_tx,
                                                       self->cancellable,
                                                       self->query,
                                                       self->result_list,
                                                       self->typeahead_delay * 1000,
                                                       worker_config,
                                                       on_worker_message_ready,
                                                       G_OBJECT (self),
                                                       &error);
      if (self->worker == NULL)
        {
          g_signal_emit_by_name (self, "stopped", 0, error);
          return;
        }
    }
  else
    {
      g_warning ("Tried to start already-running search provider %s", self->id);
    }
}

static void
force_stop (SearchProvider *provider)
{
  SearchProviderDBus *self = SEARCH_PROVIDER_DBUS (provider);
  SearchDBusWorkerMessage *message;

  g_return_if_fail (SEARCH_IS_PROVIDER_DBUS (self));

  if (self->worker == NULL)
    {
      return;
    }

  message = g_new0 (SearchDBusWorkerMessage, 1);
  message->message_type = SEARCH_DBUS_WORKER_MESSAGE_STOP;
  g_async_queue_push (self->worker_rx, message);

  /* This may cause the worker's in-progress D-Bus method call to exit early. */
  g_cancellable_cancel (self->cancellable);
}

static void
search_provider_iface_init (SearchProviderInterface *iface)
{
  iface->start = start;
  iface->force_stop = force_stop;
}
