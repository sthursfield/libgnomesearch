// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#include <glib.h>

#include "enum-types.h"
#include "search-group.h"
#include "search-provider.h"

/**
 * SearchGroup:
 *
 * When you create a search via the [class@SearchContext], it creates one or more
 * [iface@SearchProvider] instances that work in parallel. The set of providers
 * are collectively managed by a single [class@SearchGroup] instance.
 * using this class.
 */

/* Typeahead delay constants.
 *
 * We allow setting the delay to zero because the app may implement its own
 * debouncing feature. For example GtkSearchEntry does so.
 *
 * Values in milliseconds.
 */
#define MIN_TYPEAHEAD_DELAY 0
#define MAX_TYPEAHEAD_DELAY 10000
#define DEFAULT_TYPEAHEAD_DELAY 250

struct _SearchGroup
{
  GObject parent_instance;

  SearchStatus status;
  SearchQuery *query;
  gboolean typeahead;
  guint typeahead_delay;

  GHashTable *providers;
  GPtrArray *providers_running;

  GTimer *timer;
};

enum
{
  PROP_0,
  PROP_STATUS,
  PROP_QUERY,
  PROP_TYPEAHEAD,
  PROP_TYPEAHEAD_DELAY,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

G_DEFINE_FINAL_TYPE (SearchGroup, search_group, G_TYPE_OBJECT)

static gboolean
valid_state_change (SearchStatus old, SearchStatus new)
{
  switch (old)
    {
    case SEARCH_STATUS_UNKNOWN:
      return TRUE;
    case SEARCH_STATUS_CREATED:
      g_return_val_if_fail (new == SEARCH_STATUS_RUNNING || new == SEARCH_STATUS_STOPPED, FALSE);
      return TRUE;
    case SEARCH_STATUS_RUNNING:
      g_return_val_if_fail (new == SEARCH_STATUS_STOPPED, FALSE);
      return TRUE;
    case SEARCH_STATUS_STOPPED:
      g_return_val_if_fail (new == SEARCH_STATUS_RUNNING, FALSE);
      return TRUE;
    default:
      g_assert_not_reached ();
    }
}

static void
update_status (SearchGroup *self, SearchStatus status)
{
  g_return_if_fail (valid_state_change (self->status, status));

  if (status == SEARCH_STATUS_RUNNING)
    {
      g_timer_start (self->timer);

      g_signal_emit_by_name (self, "started");
    }
  else if (status == SEARCH_STATUS_STOPPED)
    {
      g_timer_stop (self->timer);

      g_signal_emit_by_name (self, "stopped");
    }

  g_object_notify (G_OBJECT (self), "status");
}

static void
for_each_provider (SearchGroup *self, GFunc func, gpointer user_data)
{
  GHashTableIter iter;
  gpointer key, value;

  g_hash_table_iter_init (&iter, self->providers);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      SearchProvider *provider = SEARCH_PROVIDER (value);

      func (provider, user_data);
    }
}

static void
on_provider_started (SearchProvider *provider, SearchGroup *self)
{
  if (self->providers_running->len == 0)
    {
      update_status (self, SEARCH_STATUS_RUNNING);
    }

  g_ptr_array_add (self->providers_running, provider);

  g_signal_emit_by_name (self, "provider-started", provider);
}

static void
on_provider_stopped (SearchProvider *provider, guint elapsed_time, GError *error, SearchGroup *self)
{
  /* The signal may be emitted twice if the user stops the search exactly when
   * it finishes searching, so we are permissive here.
   */
  if (g_ptr_array_find (self->providers_running, provider, NULL))
    {
      g_signal_emit_by_name (self, "provider-stopped", provider, elapsed_time, error);

      g_ptr_array_remove (self->providers_running, provider);
    }

  if (self->providers_running->len == 0)
    {
      update_status (self, SEARCH_STATUS_STOPPED);
    }
}

static void
add_providers (SearchGroup *self, GList *providers)
{
  for (GList *iter = providers; iter; iter = iter->next)
    {
      SearchProvider *provider = SEARCH_PROVIDER (iter->data);
      const gchar *id = search_provider_get_id (provider);

      g_hash_table_insert (self->providers, g_strdup (id), g_object_ref (provider));

      g_signal_connect_object (provider, "started", G_CALLBACK (on_provider_started), self, G_CONNECT_DEFAULT);
      g_signal_connect_object (provider, "stopped", G_CALLBACK (on_provider_stopped), self, G_CONNECT_DEFAULT);
    }
}

SearchGroup *
_search_group_new (GList *providers, SearchQuery *query, gboolean typeahead, guint typeahead_delay)
{
  SearchGroup *group;

  group = g_object_new (SEARCH_TYPE_GROUP,
                        "query", query,
                        "typeahead", typeahead,
                        "typeahead-delay", typeahead_delay,
                        NULL);

  add_providers (group, providers);

  return group;
}

static void
search_group_finalize (GObject *object)
{
  SearchGroup *self = (SearchGroup *) object;

  g_clear_pointer (&self->timer, g_timer_destroy);

  g_ptr_array_free (self->providers_running, TRUE);
  self->providers_running = NULL;

  g_clear_pointer (&self->query, search_query_free);
  g_clear_pointer (&self->providers, g_hash_table_unref);

  G_OBJECT_CLASS (search_group_parent_class)->finalize (object);
}

static void
search_group_get_property (GObject *object,
                           guint prop_id,
                           GValue *value,
                           GParamSpec *pspec)
{
  SearchGroup *self = SEARCH_GROUP (object);

  switch (prop_id)
    {
    case PROP_STATUS:
      g_value_set_enum (value, self->status);
      break;
    case PROP_QUERY:
      g_value_set_boxed (value, self->query);
      break;
    case PROP_TYPEAHEAD:
      g_value_set_boolean (value, self->typeahead);
      break;
    case PROP_TYPEAHEAD_DELAY:
      g_value_set_uint (value, self->typeahead_delay);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_group_set_property (GObject *object,
                           guint prop_id,
                           const GValue *value,
                           GParamSpec *pspec)
{
  SearchGroup *self = SEARCH_GROUP (object);

  switch (prop_id)
    {
    case PROP_STATUS:
      g_warn_if_reached ();
      break;
    case PROP_QUERY:
      self->query = search_query_copy (g_value_get_boxed (value));
      for_each_provider (self, (GFunc) search_provider_set_query, self->query);
      break;
    case PROP_TYPEAHEAD:
      self->typeahead = g_value_get_boolean (value);
      break;
    case PROP_TYPEAHEAD_DELAY:
      self->typeahead_delay = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_group_class_init (SearchGroupClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = search_group_finalize;
  object_class->get_property = search_group_get_property;
  object_class->set_property = search_group_set_property;

  g_signal_new ("started",
                SEARCH_TYPE_GROUP,
                G_SIGNAL_RUN_FIRST,
                0 /* class_offset */,
                NULL /* accumulater */,
                NULL /* accu_data */,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE,
                0);
  g_signal_new ("stopped",
                SEARCH_TYPE_GROUP,
                G_SIGNAL_RUN_FIRST,
                0 /* class_offset */,
                NULL /* accumulater */,
                NULL /* accu_data */,
                g_cclosure_marshal_generic,
                G_TYPE_NONE,
                2,
                G_TYPE_UINT,
                G_TYPE_ERROR);
  g_signal_new ("provider-started",
                SEARCH_TYPE_GROUP,
                G_SIGNAL_RUN_FIRST,
                0 /* class_offset */,
                NULL /* accumulater */,
                NULL /* accu_data */,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE,
                0);
  g_signal_new ("provider-stopped",
                SEARCH_TYPE_GROUP,
                G_SIGNAL_RUN_FIRST,
                0 /* class_offset */,
                NULL /* accumulater */,
                NULL /* accu_data */,
                g_cclosure_marshal_generic,
                G_TYPE_NONE,
                3,
                SEARCH_TYPE_PROVIDER,
                G_TYPE_UINT,
                G_TYPE_ERROR);

  properties[PROP_STATUS] =
      g_param_spec_enum ("status",
                         "Status",
                         "Search status",
                         SEARCH_TYPE_STATUS,
                         SEARCH_STATUS_UNKNOWN,
                         G_PARAM_READABLE);
  properties[PROP_QUERY] =
      g_param_spec_boxed ("query",
                          "Query",
                          "Query to search for",
                          SEARCH_TYPE_QUERY,
                          G_PARAM_READWRITE);
  properties[PROP_TYPEAHEAD] =
      g_param_spec_boolean ("typeahead",
                            "Typeahead",
                            "Allow changing search terms after search starts",
                            FALSE,
                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  properties[PROP_TYPEAHEAD_DELAY] =
      g_param_spec_uint ("typeahead-delay",
                         "Typeahead delay",
                         "Delay before running a search, when typeahead is enabled",
                         MIN_TYPEAHEAD_DELAY,
                         MAX_TYPEAHEAD_DELAY,
                         DEFAULT_TYPEAHEAD_DELAY,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
search_group_init (SearchGroup *self)
{
  self->providers = g_hash_table_new_full (g_str_hash,
                                           g_str_equal,
                                           g_free,
                                           g_object_unref);
  self->providers_running = g_ptr_array_new ();

  self->status = SEARCH_STATUS_UNKNOWN;

  self->query = NULL;

  self->typeahead = FALSE;
  self->typeahead_delay = 0;

  self->timer = g_timer_new ();
}

/**
 * search_group_get_query:
 * @self: a #SearchGroup
 *
 * Get the query associated with this search.
 *
 * Returns: (transfer full): A #SearchQuery
 */
SearchQuery *
search_group_get_query (SearchGroup *self)
{
  g_return_val_if_fail (SEARCH_IS_GROUP (self), NULL);

  return search_query_copy (self->query);
}

/**
 * search_group_get_providers:
 * @self: a #SearchGroup
 *
 * Get the provider object for each individual search that was started.
 *
 * Returns: (transfer container) (element-type SearchProvider): A #GList of #SearchProvider instances
 */
GList *
search_group_get_providers (SearchGroup *self)
{
  g_return_val_if_fail (SEARCH_IS_GROUP (self), NULL);

  return g_hash_table_get_values (self->providers);
}

/**
 * search_group_get_provider:
 * @self: a #SearchGroup
 * @provider_id: a string identifying one search provider.
 *
 * Returns: (transfer full) (allow-none): the search provider with id @provider_id, or %NULL if there is no provider with that ID.
 */
SearchProvider *
search_group_get_provider (SearchGroup *self,
                           const gchar *provider_id)
{
  SearchProvider *provider;

  g_return_val_if_fail (SEARCH_IS_GROUP (self), NULL);
  g_return_val_if_fail (provider_id != NULL, NULL);

  provider = g_hash_table_lookup (self->providers, provider_id);

  if (provider)
    {
      g_object_ref (provider);
    }

  return provider;
}

/**
 * search_group_get_result_lists:
 * @self: a #SearchGroup
 *
 * Get the [class@SearchResultList] object for each individual search that was started.
 *
 * Returns: (transfer full) (element-type SearchResultList): A #GList of SearchResultList instances
 */
GList *
search_group_get_result_lists (SearchGroup *self)
{
  GList *output = NULL;
  GHashTableIter iter;
  gpointer key, value;

  g_return_val_if_fail (SEARCH_IS_GROUP (self), NULL);

  g_hash_table_iter_init (&iter, self->providers);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      SearchProvider *provider = SEARCH_PROVIDER (value);
      SearchResultList *result_list = search_provider_get_result_list (provider);

      output = g_list_prepend (output, result_list);
    }

  return output;
}

void
search_group_set_query (SearchGroup *self, SearchQuery *query)
{
  g_return_if_fail (SEARCH_IS_GROUP (self));

  g_object_set (self, "query", query, NULL);
}

/**
 * search_group_start:
 * @self: a #SearchGroup
 *
 * Start all search providers in the group.
 *
 * This must be called exactly once.
 */
void
search_group_start (SearchGroup *self)
{
  g_return_if_fail (SEARCH_IS_GROUP (self));

  for_each_provider (self, (GFunc) search_provider_start, NULL);
}

/**
 * search_group_force_stop:
 * @self: a #SearchGroup
 *
 * Force all search providers in the group to stop work now.
 *
 * The search group cannot be started again after it has been forcibly stopped,
 * you need to create a new search group.
 */
void
search_group_force_stop (SearchGroup *self)
{
  g_return_if_fail (SEARCH_IS_GROUP (self));

  for_each_provider (self, (GFunc) search_provider_force_stop, NULL);
}
