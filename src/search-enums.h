// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * SearchStopReason:
 * @SEARCH_STOP_REASON_NO_RESULTS: No results were found for the query
 * @SEARCH_STOP_REASON_REACHED_RESULT_LIMIT: The maximum number of results was reached
 * @SEARCH_STOP_REASON_ERROR: A provider-specific error occured
 * @SEARCH_STOP_REASON_CANCELLED: The search was force-stopped by the caller.
 */
typedef enum
{
  SEARCH_STOP_REASON_NO_RESULTS,           /*< nick=no-results> */
  SEARCH_STOP_REASON_REACHED_RESULT_LIMIT, /*< nick=reached-result-limit> */
  SEARCH_STOP_REASON_ERROR,                /*< nick=error> */
  SEARCH_STOP_REASON_CANCELLED,            /*< nick=cancelled> */
} SearchStopReason;

/**
 * SearchStatus:
 * @SEARCH_STATUS_UNKNOWN: Something went wrong internally.
 * @SEARCH_STATUS_CREATED: The search was created but has not started running.
 * @SEARCH_STATUS_RUNNING: The search is in progress.
 * @SEARCH_STATUS_COMPLETED: The search completed successfully.
 * @SEARCH_STATUS_FAILED: The search failed.
 */
typedef enum
{
  SEARCH_STATUS_UNKNOWN = 0, /*< nick=unknown>*/
  SEARCH_STATUS_CREATED = 1, /*< nick=created>*/
  SEARCH_STATUS_RUNNING = 2, /*< nick=running>*/
  SEARCH_STATUS_STOPPED = 3, /*< nick=stopped>*/
} SearchStatus;

G_END_DECLS
