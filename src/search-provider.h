// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <gio/gio.h>
#include <glib-object.h>

#include "search-query.h"
#include "search-result-list.h"

G_BEGIN_DECLS

#define SEARCH_TYPE_PROVIDER (search_provider_get_type ())

G_DECLARE_INTERFACE (SearchProvider, search_provider, SEARCH, PROVIDER, GObject)

struct _SearchProviderInterface
{
  GTypeInterface parent;

  /**
   * start:
   * @self: a #SearchProvider
   *
   * Start searching for the configured query.
   *
   * This can be called exactly once for a given provider.
   */
  void (*start) (SearchProvider *self);

  /**
   * force_stop:
   * @self: a #SearchProvider
   *
   * Cancel the running search. The search cannot be started after it is cancelled.
   */
  void (*force_stop) (SearchProvider *self);
};

gchar *search_provider_get_id (SearchProvider *self);
gchar *search_provider_get_name (SearchProvider *self);
SearchQuery *search_provider_get_query (SearchProvider *self);
SearchResultList *search_provider_get_result_list (SearchProvider *self);
gboolean search_provider_get_running (SearchProvider *self);
gboolean search_provider_get_typeahead (SearchProvider *self);
guint search_provider_get_typeahead_delay (SearchProvider *self);

void search_provider_set_query (SearchProvider *self, SearchQuery *query);

void search_provider_start (SearchProvider *self);
void search_provider_force_stop (SearchProvider *self);

G_END_DECLS
