// SPDX-FileCopyrightText: 2023  Kaspar Matas
// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later

/* Worker thread for SearchProviderDBus.
 *
 * The thread is created with _search_dbus_worker_thread_start(), and from then
 * on all communication happens via message passing using GAsyncQueue..
 *
 * The thread itself is implemented as a state machine, because typeahead search
 * creates a need to cancel and restart queries and search within current results.
 */

#include <gio/gio.h>
#include <glib.h>

#include "search-dbus-worker.h"

#include "search-dbus-helpers.h"
#include "search-query.h"
#include "search-result-list.h"

const gchar *_search_dbus_worker_message_name[] = {
  "none",
  "started",
  "stopped",
  "stop",
  "set-query",
};

static const GTimeSpan MIN_RETRIGGER_INTERVAL = 50 * G_TIME_SPAN_MILLISECOND;
static const GTimeSpan MAX_RETRIGGER_INTERVAL = 10 * G_TIME_SPAN_SECOND;

typedef enum
{
  STATE_NONE = 0,
  STATE_GET_INITIAL_RESULT_SET = 1,
  STATE_GET_SUBSEARCH_RESULT_SET = 2,
  STATE_GET_RESULT_METAS = 3,
  STATE_RETRIGGER = 4,
  STATE_WAIT = 5,
  STATE_EXIT = 6,
} ThreadState;

static const gchar *thread_state_name[] = {
  "none",
  "get-initial-result-set",
  "get-subsearch-result-set",
  "get-result-metas",
  "retrigger",
  "wait",
  "exit",
};

typedef struct
{
  ThreadState state;

  GMainContext *main_context;
  GAsyncQueue *rx; /* Queue of _SearchDBusWorkerMessage */
  GAsyncQueue *tx; /* Queue of _SearchDBusWorkerMessage */
  GCancellable *force_stop;
  SearchDBusWorkerMessageReadyCallback message_ready_callback;
  GObject *message_receiver;

  SearchDBusWorkerConfig *dbus_config;

  SearchQuery *current_query;
  SearchResultList *result_list;
  GTimeSpan retrigger_interval;

  SearchQuery *next_query;
  gboolean next_query_is_subsearch;
  GDateTime *last_query_change;

  GDBusProxy *proxy;

  GPtrArray *result_ids;

  GTimer *timer;
} ThreadData;

SearchDBusWorkerConfig *
_search_dbus_worker_config_new (GDBusConnection *dbus_connection,
                               guint version,
                               const gchar *bus_name,
                               const gchar *object_path,
                               guint dbus_timeout,
                               const gchar *provider_name)
{
  SearchDBusWorkerConfig *config = g_new0 (SearchDBusWorkerConfig, 1);

  if (dbus_connection)
    {
      config->dbus_connection = g_object_ref (dbus_connection);
    }
  config->version = version;
  config->bus_name = g_strdup (bus_name);
  config->object_path = g_strdup (object_path);
  config->dbus_timeout_ms = dbus_timeout;
  config->provider_name = g_strdup (provider_name);

  return config;
}

void
_search_dbus_worker_config_free (SearchDBusWorkerConfig *config)
{
  g_clear_object (&config->dbus_connection);
  g_clear_pointer (&config->bus_name, g_free);
  g_clear_pointer (&config->object_path, g_free);
  g_clear_pointer (&config->provider_name, g_free);
  g_free (config);
}

static ThreadData *
thread_data_new (GMainContext                         *main_context,
                 GAsyncQueue                          *rx,
                 GAsyncQueue                          *tx,
                 GCancellable                         *force_stop,
                 SearchQuery                          *query,
                 SearchResultList                     *result_list,
                 guint                                 retrigger_interval,
                 SearchDBusWorkerConfig               *dbus_config,
                 SearchDBusWorkerMessageReadyCallback  message_ready_cb,
                 GObject                              *message_receiver)
{
  ThreadData *thread_data = g_new0 (ThreadData, 1);

  thread_data->state = STATE_NONE;

  thread_data->main_context = g_main_context_ref (main_context);
  thread_data->rx = g_async_queue_ref (rx);
  thread_data->tx = g_async_queue_ref (tx);
  thread_data->force_stop = g_object_ref (force_stop);
  thread_data->message_ready_callback = message_ready_cb;
  thread_data->message_receiver = g_object_ref (message_receiver);

  thread_data->current_query = search_query_copy (query);
  thread_data->result_list = g_object_ref (result_list);
  thread_data->retrigger_interval = retrigger_interval;

  thread_data->dbus_config = dbus_config;

  thread_data->next_query = NULL;
  thread_data->next_query_is_subsearch = FALSE;
  thread_data->last_query_change = NULL;

  thread_data->result_ids = NULL;

  thread_data->timer = g_timer_new ();

  return thread_data;
}

static void
thread_data_free (ThreadData *data)
{
  g_clear_pointer (&data->timer, g_timer_destroy);

  if (data->result_ids)
    {
      g_ptr_array_free (data->result_ids, TRUE);
      data->result_ids = NULL;
    }

  g_clear_object (&data->proxy);

  g_clear_object (&data->message_receiver);
  g_clear_pointer (&data->dbus_config, _search_dbus_worker_config_free);
  g_clear_pointer (&data->current_query, search_query_free);
  g_clear_object (&data->result_list);

  g_clear_pointer (&data->next_query, search_query_free);
  g_clear_pointer (&data->last_query_change, g_date_time_unref);

  g_clear_pointer (&data->rx, g_async_queue_unref);
  g_clear_pointer (&data->tx, g_async_queue_unref);
  g_clear_object (&data->force_stop);

  g_free (data);
}

static void
ensure_retrigger_interval (GDateTime *last_query_change, GTimeSpan retrigger_interval)
{
  g_autoptr (GDateTime) now = g_date_time_new_now_utc ();
  GTimeSpan timespan = g_date_time_difference (now, last_query_change);

  if (timespan < retrigger_interval)
    {
      g_usleep (retrigger_interval - timespan);
    }
}

/* We have a separate data structure for the send_message_ready callback, because
 * it may be called after the thread itself has exited and freed the ThreadData.
 */
typedef struct {
  SearchDBusWorkerMessageReadyCallback callback;
  GObject *receiver;
} MessageReadyData;

static MessageReadyData *
message_ready_data_new (SearchDBusWorkerMessageReadyCallback callback,
                        GObject *message_receiver)
{
  MessageReadyData *data = g_new0 (MessageReadyData, 1);
  data->callback = callback;
  data->receiver = g_object_ref (message_receiver);
  return data;
}

static void
message_ready_data_free (MessageReadyData *data)
{
  g_clear_object (&data->receiver);
  g_free (data);
}

static gboolean send_message_ready (gpointer user_data)
{
  MessageReadyData *data = user_data;

  data->callback (data->receiver);

  return G_SOURCE_REMOVE;
}

static void
send_message (ThreadData              *data,
              SearchDBusWorkerMessage *message)
{
  MessageReadyData *message_ready_data;

  message_ready_data = message_ready_data_new (data->message_ready_callback, data->message_receiver);

  g_async_queue_push (data->tx, message);
  g_main_context_invoke_full (data->main_context, G_PRIORITY_DEFAULT, send_message_ready, message_ready_data, (GDestroyNotify) message_ready_data_free);
}

static void
send_started (ThreadData *data)
{
  SearchDBusWorkerMessage *message = g_new0 (SearchDBusWorkerMessage, 1);

  message->message_type = SEARCH_DBUS_WORKER_MESSAGE_STARTED;
  send_message (data, message);
}

static void
send_stopped (ThreadData *data, SearchStopReason stop_reason, GError *error)
{
  SearchDBusWorkerMessage *message = g_new0 (SearchDBusWorkerMessage, 1);

  message->message_type = SEARCH_DBUS_WORKER_MESSAGE_STOPPED;
  message->stopped_data.stop_reason = stop_reason;
  message->stopped_data.elapsed_time = g_timer_elapsed (data->timer, NULL);
  message->stopped_data.error = error;
  send_message (data, message);
}

static void
to_start_state (ThreadData *data)
{
  g_assert (data->state == STATE_NONE);

  data->last_query_change = g_date_time_new_now_utc ();
  data->state = STATE_GET_INITIAL_RESULT_SET;

  send_started (data);
}

static void
to_wait_state (ThreadData *data, SearchStopReason stop_reason, GError *error)
{
  send_stopped (data, stop_reason, error);

  data->state = STATE_WAIT;
}

static void
to_exit_state (ThreadData *data, SearchStopReason stop_reason, GError *error)
{
  send_stopped (data, stop_reason, error);

  data->state = STATE_EXIT;
}

/* Select a limited set of results from 'results', return a new (floating) variant */
static GVariant *
select_results (GVariant *results, gint limit, GError **error)
{
  g_return_val_if_fail (g_variant_is_of_type (results, G_VARIANT_TYPE ("as")), NULL);

  gsize n_results = g_variant_n_children (results);
  if (limit > 0 && n_results > limit)
    {
      n_results = limit;
    }

  gchar **variants = g_new (gchar *, n_results);
  for (gsize i = 0; i < n_results; i++)
    {
      GVariant *string_value = g_variant_get_child_value (results, i);
      const gchar *string = g_variant_get_string (string_value, NULL);
      variants[i] = g_strdup (string); // Duplicate the string
      g_variant_unref (string_value);
    }

  // Create a new 'as' GVariant with the array of strings
  GVariant *limited_results = g_variant_new_strv ((const gchar *const *) variants, n_results);

  // Free the duplicated strings and the array
  for (gsize i = 0; i < n_results; i++)
    {
      g_free (variants[i]);
    }
  g_free (variants);

  return limited_results;
}

static void
populate_results_list (SearchResultList *result_list,
                       GVariant *metas,
                       const gchar *provider_name)
{
  GVariantIter metas_iter;
  GVariant *meta = NULL;

  g_return_if_fail (g_variant_is_of_type (metas, G_VARIANT_TYPE ("aa{sv}")));

  g_variant_iter_init (&metas_iter, metas);

  /* FIXME: We could be cleverer here, in case of subsearches, and reuse
   * results that are already in the list.
   */
  search_result_list_remove_all (result_list);

  while ((meta = g_variant_iter_next_value (&metas_iter)) != NULL)
    {
      g_autofree gchar *id = NULL;
      g_autofree gchar *name = NULL;
      g_autofree gchar *description = NULL;
      g_autoptr (GVariant) icon_value = NULL;
      g_autoptr (GIcon) icon = NULL;
      g_autoptr (SearchResultIconData) icon_data = NULL;
      SearchResult *result = NULL;
      g_auto (GVariantDict) dict = G_VARIANT_DICT_INIT (NULL);

      g_assert (g_variant_is_of_type (meta, G_VARIANT_TYPE ("a{sv}")));
      g_variant_dict_init (&dict, meta);

      if (!g_variant_dict_lookup (&dict, "id", "s", &id))
        {
          g_warning ("Invalid search result: missing 'id' field");
          continue;
        };

      if (!g_variant_dict_lookup (&dict, "name", "s", &name))
        {
          g_warning ("Invalid search result: missing 'name' field");
          continue;
        };

      if ((icon_value = g_variant_dict_lookup_value (&dict, "gicon", NULL)))
        {
          icon = g_icon_deserialize (icon_value);
        }
      else if ((icon_value = g_variant_dict_lookup_value (&dict, "icon-data", NULL)))
        {
          icon_data = search_result_icon_data_new_from_variant (icon_value);
        }

      g_variant_dict_lookup (&dict, "description", "s", &description);

      result = search_result_new (id, name, icon, icon_data, description, NULL /* clipboard_text */, provider_name);
      search_result_list_append (result_list, result);
      g_object_unref (result);

      g_variant_unref (meta);
    }
}

static void
store_result_ids (ThreadData *data, GVariant *result_ids_variant)
{
  const gchar **result_ids;
  gsize result_ids_length;

  result_ids = g_variant_get_strv (result_ids_variant, &result_ids_length);

  if (data->result_ids != NULL)
    {
      g_ptr_array_free (data->result_ids, TRUE);
    }

  /* We could use g_ptr_array_new_from_array() here, it requires GLib 2.76 */
  data->result_ids = g_ptr_array_new_full (result_ids_length, g_free);

  for (int i = 0; i < result_ids_length; i++)
    {
      g_ptr_array_add (data->result_ids, g_strdup (result_ids[i]));
    }
  g_free (result_ids);
}

static void
run_state_get_initial_result_set (ThreadData *data)
{
  g_autoptr (GVariant) results = NULL;
  g_autoptr (GVariant) limited_results = NULL;
  GError *error = NULL;

  results = _search_dbus_call_get_initial_result_set (data->proxy,
                                                      search_query_text_get_text (data->current_query),
                                                      data->dbus_config->dbus_timeout_ms,
                                                      data->force_stop,
                                                      &error);

  if (g_cancellable_is_cancelled (data->force_stop))
    {
      to_exit_state (data, SEARCH_STOP_REASON_CANCELLED, NULL);
      return;
    }

  if (!results || error != NULL)
    {
      to_exit_state (data, SEARCH_STOP_REASON_ERROR, error);
      return;
    }

  if (g_variant_n_children (results) == 0)
    {
      search_result_list_remove_all (data->result_list);
      to_wait_state (data, SEARCH_STOP_REASON_NO_RESULTS, NULL);
      return;
    }

  gint limit = 5; /* FIXME: should be configurable */
  limited_results = select_results (results, limit, &error);

  if (!limited_results)
    {
      to_exit_state (data, SEARCH_STOP_REASON_ERROR, error);
      return;
    }

  g_variant_ref_sink (limited_results);
  store_result_ids (data, limited_results);

  data->state = STATE_GET_RESULT_METAS;
}

static void
run_state_get_subsearch_result_set (ThreadData *data)
{
  g_autoptr (GVariant) results = NULL;
  GError *error = NULL;

  results = _search_dbus_call_get_subsearch_result_set (data->proxy,
                                                        search_query_text_get_text (data->current_query),
                                                        data->result_ids,
                                                        data->dbus_config->dbus_timeout_ms,
                                                        data->force_stop,
                                                        &error);

  if (g_cancellable_is_cancelled (data->force_stop))
    {
      to_exit_state (data, SEARCH_STOP_REASON_CANCELLED, NULL);
      return;
    }

  if (!results || error != NULL)
    {
      to_exit_state (data, SEARCH_STOP_REASON_ERROR, error);
      return;
    }

  if (g_variant_n_children (results) == 0)
    {
      search_result_list_remove_all (data->result_list);
      to_wait_state (data, SEARCH_STOP_REASON_NO_RESULTS, NULL);
      return;
    }

  store_result_ids (data, results);

  data->state = STATE_GET_RESULT_METAS;
}

static void
run_state_get_result_metas (ThreadData *data)
{
  g_autoptr (GVariant) metas = NULL;
  GError *error = NULL;

  metas = _search_dbus_call_get_result_metas (data->proxy, data->result_ids, data->dbus_config->dbus_timeout_ms, data->force_stop, &error);

  if (g_cancellable_is_cancelled (data->force_stop))
    {
      to_exit_state (data, SEARCH_STOP_REASON_CANCELLED, NULL);
      return;
    }

  if (!metas || error != NULL)
    {
      to_exit_state (data, SEARCH_STOP_REASON_ERROR, error);
      return;
    }

  populate_results_list (data->result_list, metas, data->dbus_config->provider_name);

  to_wait_state (data, SEARCH_STOP_REASON_REACHED_RESULT_LIMIT, NULL);
}

static void
run_state_retrigger (ThreadData *data)
{
  ensure_retrigger_interval (data->last_query_change, data->retrigger_interval);

  g_clear_pointer (&data->last_query_change, g_date_time_unref);
  data->last_query_change = g_date_time_new_now_utc ();

  g_clear_pointer (&data->current_query, search_query_free);
  data->current_query = data->next_query;
  data->next_query = NULL;

  data->state = data->next_query_is_subsearch ? STATE_GET_SUBSEARCH_RESULT_SET : STATE_GET_INITIAL_RESULT_SET;
}

static void
process_message (ThreadData *data, SearchDBusWorkerMessage *message)
{
  g_debug ("Worker thread: process message %s", _search_dbus_worker_message_name[message->message_type]);

  switch (message->message_type)
    {
    case SEARCH_DBUS_WORKER_MESSAGE_SET_QUERY:
      g_clear_pointer (&data->next_query, search_query_free);

      data->next_query_is_subsearch = search_query_is_subquery (data->current_query, message->set_query_data.query);
      data->next_query = message->set_query_data.query;

      if (g_cancellable_is_cancelled (data->force_stop) || data->state == STATE_EXIT)
        {
          /* The thread won't actually process this query as it is stopping. We just needed
           * to free the memory allocated to the message.
           */
          data->state = STATE_EXIT;
        }
      else
        {
          data->state = STATE_RETRIGGER;
        }
      break;

    case SEARCH_DBUS_WORKER_MESSAGE_STOP:
      to_exit_state (data, SEARCH_STOP_REASON_CANCELLED, NULL);
      break;

    case SEARCH_DBUS_WORKER_MESSAGE_STARTED:
    case SEARCH_DBUS_WORKER_MESSAGE_STOPPED:
    default:
      g_assert_not_reached ();
      break;
    };

  g_free (message);
}

static void
empty_message_queue (ThreadData *data)
{
  SearchDBusWorkerMessage *message;

  while ((message = g_async_queue_try_pop (data->rx)))
    {
      process_message (data, message);
    }
}

static gpointer
thread_func (gpointer user_data)
{
  ThreadData *data = user_data;
  SearchDBusWorkerMessage *message;
  GError *error = NULL;

  to_start_state (data);

  data->proxy = _search_dbus_get_org_gnome_shell_search_provider_dbus_proxy (data->dbus_config->dbus_connection,
                                                                             data->dbus_config->version,
                                                                             data->dbus_config->bus_name,
                                                                             data->dbus_config->object_path,
                                                                             &error);

  if (data->proxy == NULL)
    {
      to_exit_state (data, SEARCH_STOP_REASON_ERROR, error);
    }

  do
    {
      g_debug ("Worker thread: Run state %s", thread_state_name[data->state]);

      switch (data->state)
        {
        case STATE_GET_INITIAL_RESULT_SET:
          run_state_get_initial_result_set (data);
          break;

        case STATE_GET_SUBSEARCH_RESULT_SET:
          run_state_get_subsearch_result_set (data);
          break;

        case STATE_GET_RESULT_METAS:
          run_state_get_result_metas (data);
          break;

        case STATE_RETRIGGER:
          run_state_retrigger (data);
          break;

        case STATE_WAIT:
          message = g_async_queue_pop (data->rx);

          send_started (data);
          process_message (data, message);
          break;

        case STATE_EXIT:
          break;

        case STATE_NONE:
        default:
          g_assert_not_reached ();
          break;
        }

      if (data->state == STATE_EXIT)
        {
          break;
        }

      while ((message = g_async_queue_try_pop (data->rx)))
        {
          process_message (data, message);
        }
    }
  while (data->state != STATE_EXIT);

  g_debug ("Worker thread: exiting");
  empty_message_queue (data);

  thread_data_free (data);

  return NULL;
}

/* Create a new D-Bus worker thread.
 *
 * The thread takes ownership of all arguments.
 *
 * If the thread cannot be created, returns %NULL and sets @error.
 */
GThread *
_search_dbus_worker_thread_start (GMainContext *main_context,
                                  GAsyncQueue *rx,
                                  GAsyncQueue *tx,
                                  GCancellable *cancellable,
                                  SearchQuery *query,
                                  SearchResultList *result_list,
                                  GTimeSpan retrigger_interval,
                                  SearchDBusWorkerConfig *dbus_config,
                                  SearchDBusWorkerMessageReadyCallback message_ready_cb,
                                  GObject *message_receiver,
                                  GError **error)
{
  ThreadData *thread_data;
  GThread *thread;

  g_return_val_if_fail (retrigger_interval >= MIN_RETRIGGER_INTERVAL, NULL);
  g_return_val_if_fail (retrigger_interval <= MAX_RETRIGGER_INTERVAL, NULL);

  g_return_val_if_fail (message_ready_cb != NULL, NULL);
  g_return_val_if_fail (G_IS_OBJECT (message_receiver), NULL);

  thread_data = thread_data_new (main_context, rx, tx, cancellable, query, result_list, retrigger_interval, dbus_config, message_ready_cb, message_receiver);

  thread = g_thread_try_new ("search-dbus-worker", thread_func, thread_data, error);

  if (!thread)
    {
      thread_data_free (thread_data);
      return NULL;
    }

  return thread;
}
