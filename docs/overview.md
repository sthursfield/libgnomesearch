Title: Overview

# GNOME Search library

`libgnomesearch` is a library to run simple searches within the GNOME desktop
environment. The aim is to provide a single high-level API to access the
various search providers that exist within GNOME.

## Rationale

[GNOME Shell](https://gitlab.gnome.org/GNOME/gnome-shell/) introduced a design for
desktop search where multiple [search providers](https://developer.gnome.org/documentation/tutorials/search-provider.html)
produce search results. This has been working well since 2012, and is easy to
extend - any system process can add a search provider without needing
to change the GNOME Shell code.

The drawbacks to this design are:

  * the search controller is part of the GNOME Shell JavaScript UI code and
    cannot be easily tested and reused in other places.
  * efficiency is not good - each search provider is a separate process, so many
    context-switches happen whenever the search terms change.

The first goal of `libgnomesearch` is to re-implement the search controller logic
in a reusable shared library, replacing the current JavaScript implementation that
lives in [gnome-shell.git](https://gitlab.gnome.org/GNOME/gnome-shell/).

The second goal is to integrate some of the search providers from the GNOME core
apps directly into the library, so that the Shell can get results without
excessive context-switching. Core search providers that we might integrate include:

  * indexed filesystem search, powered by [Tracker Miner FS](https://gitlab.gnome.org/GNOME/tracker-miners/),
    and currently implemented in the Nautilus search provider
  * the calculator "search", currently implemented in GNOME Calculator.
 
Hopefully this initiative will unlock more code reuse, wider testing and new innovations
in desktop search.

## How to use

Start by defining a search query. The [struct@SearchQuery] data structure aims to represent
simple and complex queries in an unambigious way.

To start a search, create a [class@SearchContext] object and call [method@SearchContext.create_oneshot_search]
or [method@SearchContext.create_typeahead_search]. The search can be monitored and controlled with the returned
[class@SearchGroup] object. This is a wrapper around multiple **search providers**, which each implement the [iface@SearchProvider] interface.
The search will not start until you call [method@SearchGroup.start], first you can connect to completion signals.

A *oneshot* search is suitable for commandline-style interfaces which run the search once and then exit.
In this case, you would connect to the [signal@SearchGroup::stopped] signal to be notified when the overall search is complete.
You might also connect to [signal@SearchGroup::provider_stopped] to be notified each time a provider has finished
producing results.

A *typeahead* search is useful in interactive applications where the user can change the search terms
while the search is in progress. Search results are made available in [class@SearchResultList] objects,
which implement the [class@GIO.ListModel] interface, so you can display them easily using GtkListView
and other list display widgets.

Here's a basic example of running a search in C:

```
void run_search(void) {
  g_autoptr (SearchQuery) query = NULL;
  g_autoptr (SearchGroup) group = NULL;

  query = search_query_new_text ("Search for this phrase");

  group = search_context_create_oneshot_search (context, query);

  g_signal_connect (group, "provider-stopped", G_CALLBACK (search_provider_finish_cb), NULL);
  g_signal_connect (group, "stopped", G_CALLBACK (search_group_finish_cb), NULL);

  search_group_start (group);
}
```

This example would be useful in a commandline tool that prints a group of search results in
`search_provider_finish_cb`, and exits the process in `search_group_finish_cb`
when all searches have finished running.
