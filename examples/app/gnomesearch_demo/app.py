# SPDX-FileCopyrightText: 2023  Kaspar Matas
# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version("Search", "0.1")
from gi.repository import Adw, Gdk, Gtk, Gio, GLib

import logging
import sys

from .app_window import AppWindow

LOG = logging.getLogger(__name__)


class Application(Adw.Application):
    def __init__(self, application_id, version):
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

        super().__init__(
            application_id=application_id, flags=Gio.ApplicationFlags.FLAGS_NONE
        )
        self.props.resource_base_path = "/libgnomesearch/Example"
        GLib.set_application_name("libgnomesearch demo")
        GLib.set_prgname(application_id)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_file(
            Gio.File.new_for_uri("resource:///libgnomesearch/Example/app.css")
        )
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        self._version = version
        self._window = None

    def _quit(self, action=None, param=None):
        self._window.destroy()

    def do_activate(self):
        if not self._window:
            self._window = AppWindow(self)
        self._window.present()
