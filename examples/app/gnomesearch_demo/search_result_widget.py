# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version("Search", "0.1")
from gi.repository import Gtk, GObject, Search

import logging

LOG = logging.getLogger(__name__)


@Gtk.Template(resource_path="/libgnomesearch/Example/search_result_widget.ui")
class SearchResultWidget(Gtk.Box):
    """Widget for use with GtkListView to display a single search result."""

    __gtype_name__ = "SearchResultWidget"

    provider_name = Gtk.Template.Child("provider_name")
    name = Gtk.Template.Child("name")
    description = Gtk.Template.Child("description")
    icon = Gtk.Template.Child("icon")

    def bind(self, search_result: Search.Result):
        self.provider_name.set_text(search_result.get_provider_name())
        self.name.set_text(search_result.get_name())
        self.description.set_text(search_result.get_description() or "")
        if search_result.get_icon():
          self.icon.set_from_gicon(search_result.get_icon())
        elif search_result.get_icon_data():
          icon_data = search_result.get_icon_data()
          pixbuf = Gdk.Pixbuf.new_from_bytes (icon_data.get_bytes(),
                                              Gdk.ColorSpace.RGB,
                                              icon_data.get_has_alpha(),
                                              icon_data.get_bits_per_sample(),
                                              icon_data.get_width(),
                                              icon_data.get_height(),
                                              icon_data.get_rowstride())
          self.icon.set_from_pixbuf (pixbuf)

    def unbind(self):
        self.name.set_text("")
        self.description.set_text("")
        self.icon.clear()


def search_result_widget_factory() -> Gtk.ListItemFactory:
    """Create a Gtk.SignalListItemFactory for use with SearchResultWidget.

    Use this function to create a factory for GtkListView.set_factory().

    """

    def on_setup(factory: Gtk.SignalListItemFactory, obj: GObject.Object):
        list_item_widget = SearchResultWidget()
        obj.set_child(list_item_widget)

    def on_bind(factory: Gtk.SignalListItemFactory, obj: GObject.Object):
        list_item_widget = obj.get_child()
        list_item_widget.bind(obj.get_item())

    def on_unbind(factory: Gtk.SignalListItemFactory, obj: GObject.Object):
        list_item_widget = obj.get_child()
        list_item_widget.unbind()

    def on_teardown(factory: Gtk.SignalListItemFactory, obj: GObject.Object):
        obj.set_child(None)

    factory = Gtk.SignalListItemFactory.new()

    factory.connect("setup", on_setup)
    factory.connect("bind", on_bind)
    factory.connect("unbind", on_unbind)
    factory.connect("teardown", on_teardown)

    return factory
