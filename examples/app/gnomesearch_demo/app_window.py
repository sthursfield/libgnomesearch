# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version("Search", "0.1")
from gi.repository import Gtk, Gio, Search

import logging

from .search_group_result_list import SearchGroupResultList
from .search_result_widget import search_result_widget_factory

LOG = logging.getLogger(__name__)


@Gtk.Template(resource_path="/libgnomesearch/Example/app_window.ui")
class AppWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "ExampleWindow"

    search_entry = Gtk.Template.Child("search_entry")
    results_view = Gtk.Template.Child("results_view")

    def __init__(self, app):
        """Initialize the main window.

        :param Gtk.Application app: Application object
        """
        super().__init__(application=app, title="libgnomesearch demo")

        self.search_entry.connect("search-changed", self.on_search_changed)

        self.search_context = Search.Context()

        # FIXME: workaround for hang
        conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        self.search_context.set_dbus_connection(conn)

        self.search = None

    def on_search_changed(self, search_entry):
        text = search_entry.get_text()

        if text:
            query = Search.Query.new_text(text)

            if self.search is None:
                self.search = self.search_context.create_typeahead_search(query)

                self.group_result_list = SearchGroupResultList(
                    self.search.get_result_lists()
                )
                selection_model = Gtk.SingleSelection.new(self.group_result_list)
                self.results_view.set_factory(search_result_widget_factory())
                self.results_view.set_model(selection_model)

                self.search.start()
            else:
                self.search.set_query(query)
        else:
            if self.search:
                self.search.force_stop()
                self.search = None
