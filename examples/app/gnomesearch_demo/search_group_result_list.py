# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version("Search", "0.1")
from gi.repository import Adw, Gdk, Gtk, Gio, GLib, GObject, Search

import logging

LOG = logging.getLogger(__name__)


class SearchGroupResultList(GObject.Object, Gio.ListModel):
    """Result list that appends multiple list models together."""

    def __init__(self, result_lists):
        super(SearchGroupResultList, self).__init__()

        self._store = Gio.ListStore.new(Search.Result)

        self._result_lists = result_lists
        LOG.debug("Watching %i result lists", len(result_lists))

        self._offsets = [0] * len(self._result_lists)

        position = 0
        for list_index, result_list in enumerate(result_lists):
            result_list.connect("items-changed", self._on_child_items_changed)

            self._offsets[list_index] = self.get_n_items()
            for i in range(0, result_list.get_n_items()):
                item = result_list.get_item(i)
                self._store.append(item)

    def _on_child_items_changed(
        self, result_list: Gio.ListModel, position: int, removed: int, added: int
    ):
        assert result_list in self._result_lists

        list_index = self._result_lists.index(result_list)
        LOG.debug(
            "child[%i]: Got items-changed: %i, %i, %i",
            list_index,
            position,
            removed,
            added,
        )

        # Process the items for this list
        offset = self._offsets[list_index]

        additions = []
        for n in range(0, added):
            item = result_list.get_item(position + n)
            additions.append(item)

        LOG.debug(
            "child[%i]: Splicing main list model: %i, %i, %s",
            list_index,
            position + offset,
            removed,
            additions,
        )
        self._store.splice(
            position + offset,
            removed,
            additions,
        )

        # Update the offsets
        for i in range(list_index + 1, len(self._result_lists)):
            self._offsets[i] = self._offsets[i] - removed + added

        LOG.debug(
            "Emitting items-changed: %i, %i, %i", offset + position, removed, added
        )
        self.emit("items-changed", offset + position, removed, added)

    def do_get_item_type(self):
        return Search.Result

    def do_get_n_items(self) -> int:
        return self._store.get_n_items()

    def do_get_item(self, position) -> Search.Result:
        return self._store.get_item(position)
