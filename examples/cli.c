/* main.c
 *
 * Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "libgnomesearch.h"
#include <gio/gio.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>

static GMainLoop *main_loop;

static void
search_group_stopped_cb (SearchGroup *group)
{
  g_main_loop_quit (main_loop);
}

static void
search_provider_stopped_cb (SearchGroup *group,
                            SearchProvider *provider,
                            guint elapsed_time,
                            GError *error,
                            gpointer user_data)
{
  g_autofree gchar *provider_id = NULL;
  g_autoptr (SearchResultList) result_list = NULL;
  guint n_results;

  provider_id = search_provider_get_id (provider);

  if (error)
    {
      g_print ("Search provider %s failed: %s", provider_id, error->message);

      return;
    }

  result_list = search_provider_get_result_list (provider);

  n_results = g_list_model_get_n_items (G_LIST_MODEL (result_list));

  g_print ("\nResults for %s (time taken: %.02fs):\n",
           provider_id, (float) elapsed_time / 1000.0);

  if (n_results == 0)
    {
      g_print ("  None\n");
    }

  for (guint i = 0; i < n_results; i++)
    {
      g_autoptr (SearchResult) result = g_list_model_get_item (G_LIST_MODEL (result_list), i);

      const gchar *description;

      g_print ("  %02i. %s\n", i, search_result_get_id (result));
      g_print ("    Name: %s\n", search_result_get_name (result));

      description = search_result_get_description (result);
      if (description)
        {
          g_print ("    Description: %s\n", description);
        }
    }
}

static void
run_search (SearchContext *context, const gchar *text, gint limit)
{
  g_autoptr (SearchQuery) query = NULL;
  g_autoptr (SearchGroup) group = NULL;

  query = search_query_new_text (text);

  group = search_context_create_oneshot_search (context, query);

  g_signal_connect (group, "provider-stopped", G_CALLBACK (search_provider_stopped_cb), NULL);
  g_signal_connect (group, "stopped", G_CALLBACK (search_group_stopped_cb), NULL);

  search_group_start (group);

  g_main_loop_run (main_loop);
}

int
main (void)
{
  g_autoptr (SearchContext) context = NULL;
  GList *providers = NULL;
  SearchProviderDBus *provider = NULL;

  main_loop = g_main_loop_new (NULL, FALSE);

  context = search_context_new ();

  providers = search_context_load_providers (context, FALSE);
  if (providers != NULL)
    {
      g_print ("Found the following service providers:\n");

      for (GList *iter = providers; iter != NULL; iter = iter->next)
        {
          provider = iter->data;
          g_print ("BusName: %s, ObjectPath: %s, DesktopId: %s, Version: %d\n",
                   search_provider_dbus_get_bus_name (provider),
                   search_provider_dbus_get_object_path (provider),
                   search_provider_dbus_get_desktop_id (provider),
                   search_provider_dbus_get_version (provider));
        }

      // Hardcoded for now
      const gchar *search_text = "Settings";
      gint limit = 5;
      run_search (context, search_text, limit);
    }
  else
    {
      g_print ("No service providers found.\n");
    }

  g_main_loop_unref (main_loop);

  return 0;
}
