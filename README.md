# libgnomesearch

![coverage](https://gitlab.gnome.org/sthursfield/libgnomesearch/badges/main/coverage.svg?job=coverage)

Current requirements listed [HERE](https://gitlab.gnome.org/GNOME/tracker/-/issues/150#note_1614885)

## Documentation

The API reference is [here](https://sthursfield.pages.gitlab.gnome.org/libgnomesearch/libgnomesearch-0.1/).

## Project Structure

The project is organized as follows:

- `example`: Contains a CLI main program demonstrating how to use the library.
- `include`: Contains the library's header files.
- `src`: Contains the library's source files.
- `tests`: Contains unit tests for the library.
- `scripts`: Contains helper scripts for building, testing, and running the example.

## Scripts

The following scripts are available in the `scripts` directory:

- `build_everything.sh`: Builds the library, example, and tests, and installs the library (for debugging the script's release target has to be altered to debug).
- `build_library_only.sh`: Builds and installs the library only.
- `clang_checks.sh`: Performs code analysis using Clang.
- `get_meson_build.sh`: Downloads and installs the Meson build system if not already installed.
- `run_example.sh`: Runs the example CLI main program.
- `run_unit_tests.sh`: Runs the unit tests for the library.

## Example CLI Main Program

The example directory contains a CLI main program (`main.c`) that demonstrates how to use the LibGnomeSearch library. To build and run the example:

1. Run the `build_everything.sh` script from the `scripts` directory.
2. Run the `run_example.sh` script from the `scripts` directory.

Optionally a python script can also be used to test the functionality of the search library functions as demonstrated in `main.py`

## Development Setup for Visual Studio Code

To set up the development environment for Visual Studio Code, follow these steps:

1. Install the C/C++ extension for Visual Studio Code if you haven't already.
2. Run the `build_everything.sh` script from the `scripts` directory to build and install the library.
3. In the `.vscode` folder, create a `c_cpp_properties.json` file with the following configuration:

```json
{
    "configurations": [
        {
            ...
            "includePath": [
                "${workspaceFolder}/**",
                "/usr/local/include/libgnomesearch"
            ],
            ...
        }
    ],
    "version": 4
}
```

This configuration specifies the include paths for your project and other settings, such as the compiler path, C and C++ standards, and IntelliSense mode.

Then restart Visual Studio Code to apply the changes.

Now Visual Studio Code should be able to resolve the #include errors, and you should have a better development experience.
