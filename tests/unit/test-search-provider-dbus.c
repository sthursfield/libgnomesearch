/* Copyright 2023 Kaspar Matas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "libgnomesearch.h"

#include <glib.h>
#include <glib/gstdio.h>

// Add more tests in the future

typedef struct
{
  SearchContext *context;
  gchar *temp_search_provider_dir;
} SearchFixture;

static char *
create_test_search_providers (void)
{
  gchar *temp_search_provider_dir = NULL;

  temp_search_provider_dir = g_dir_make_tmp ("test_search_providers_XXXXXX", NULL);

  const gchar *test_provider_data[] = { "[Shell Search Provider]\n"
                                        "BusName=org.gnome.TestSearchProvider1\n"
                                        "ObjectPath=/org/gnome/TestSearchProvider1\n"
                                        "DesktopId=test-search-provider1.desktop\n"
                                        "Version=1\n",

                                        "[Shell Search Provider]\n"
                                        "BusName=org.gnome.TestSearchProvider2\n"
                                        "ObjectPath=/org/gnome/TestSearchProvider2\n"
                                        "DesktopId=test-search-provider2.desktop\n"
                                        "Version=1\n",
                                        NULL };

  for (int i = 0; test_provider_data[i] != NULL; i++)
    {
      gchar *filename = g_strdup_printf ("%s/test-search-provider%d.ini",
                                         temp_search_provider_dir, i + 1);
      g_file_set_contents (filename, test_provider_data[i], -1, NULL);
      g_free (filename);
    }

  return temp_search_provider_dir;
}

static void
setup (SearchFixture *fixture, gconstpointer user_data)
{
  fixture->temp_search_provider_dir = create_test_search_providers ();

  fixture->context = search_context_new ();
  search_context_set_search_provider_dir (fixture->context, fixture->temp_search_provider_dir);
}

static void
teardown (SearchFixture *fixture, gconstpointer user_data)
{
  g_object_unref (fixture->context);

  g_rmdir (fixture->temp_search_provider_dir);
  g_free (fixture->temp_search_provider_dir);
}

static void
test_list_search_providers (SearchFixture *fixture,
                            gconstpointer user_data)
{
  GList *providers = search_context_load_providers (fixture->context, FALSE);

  g_assert_nonnull (providers);
  g_assert_cmpint (g_list_length (providers), >, 0);

  for (GList *iter = providers; iter != NULL; iter = iter->next)
    {
      SearchProviderDBus *provider = iter->data;
      g_assert_nonnull (
          search_provider_dbus_get_bus_name (provider));
      g_assert_nonnull (
          search_provider_dbus_get_object_path (provider));
      g_assert_nonnull (
          search_provider_dbus_get_desktop_id (provider));
      gint version = search_provider_dbus_get_version (provider);
      g_assert_true (version > 0);
    }
}

gint
main (gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add (
      "/Search/unit/test_load_remote_search_providers",
      SearchFixture, NULL, setup, test_list_search_providers, teardown);

  return g_test_run ();
}
