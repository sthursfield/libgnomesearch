// SPDX-FileCopyrightText: 2023  Sam Thursfield
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Test for the internal SearchDBusWorker module.
//
// Uses GTestDBus and basic_dbus_search_provider.py.

#include "libgnomesearch.h"
#include "search-dbus-worker.h"

#include <glib.h>
#include <gio/gio.h>

const guint BASIC_SEARCH_PROVIDER_VERSION = 2;
const gchar *BASIC_SEARCH_PROVIDER_BUS_NAME = "org.gnome.gitlab.sthursfield.libgnomesearch.BasicSearchProvider";
const gchar *BASIC_SEARCH_PROVIDER_OBJECT_PATH = "/org/gnome/gitlab/sthursfield/libgnomesearch/BasicSearchProvider";
const guint DBUS_TIMEOUT_MS = 250;

typedef struct
{
  GAsyncQueue *worker_rx;
  GAsyncQueue *worker_tx;
  GCancellable *force_stop;
  SearchQuery *query;
  SearchResultList *result_list;
} DBusWorkerFixture;

static void
setup (DBusWorkerFixture *fixture, gconstpointer user_data)
{
  fixture->worker_rx = g_async_queue_new ();
  fixture->worker_tx = g_async_queue_new ();
  fixture->force_stop = g_cancellable_new ();
  fixture->query = search_query_new_text ("process");
  fixture->result_list = search_result_list_new ();
}

static void
teardown (DBusWorkerFixture *fixture, gconstpointer user_data)
{
  g_clear_pointer (&fixture->worker_rx, g_async_queue_unref);
  g_clear_pointer (&fixture->worker_tx, g_async_queue_unref);
  g_clear_object (&fixture->force_stop);
  g_clear_pointer (&fixture->query, search_query_free);
  g_clear_object (&fixture->result_list);
}

static
void message_ready_cb (gpointer user_data)
{
  GObject *dummy = G_OBJECT (user_data);
  GAsyncQueue *worker_tx = g_object_get_data (dummy, "worker_tx");
  SearchDBusWorkerMessage *message;

  g_print ("Got message ready\n");
  message = g_async_queue_pop (worker_tx);
  switch (message->message_type)
    {
    case SEARCH_DBUS_WORKER_MESSAGE_STARTED:
      g_print ("Got started\n");
      break;
    case SEARCH_DBUS_WORKER_MESSAGE_STOPPED:
      g_print ("Got stopped\n");
      if (message->stopped_data.error)
        {
          g_print ("Error: %s", message->stopped_data.error->message);
          g_error_free (message->stopped_data.error);
        }
      break;
    case SEARCH_DBUS_WORKER_MESSAGE_SET_QUERY:
    case SEARCH_DBUS_WORKER_MESSAGE_STOP:
    default:
      g_assert_not_reached ();
    }
  g_free (message);
}

static void
test_dbus_worker (DBusWorkerFixture *fixture,
                  gconstpointer user_data)
{
  GDBusConnection *dbus_connection;
  g_autoptr (GMainContext) main_context = g_main_context_ref_thread_default ();
  SearchDBusWorkerConfig *config;
  GThread *worker;
  GError *error = NULL;

  dbus_connection = NULL; /* Use session bus, which is overwritten by GTestDBus */
  config = _search_dbus_worker_config_new (dbus_connection,
                                           BASIC_SEARCH_PROVIDER_VERSION,
                                           BASIC_SEARCH_PROVIDER_BUS_NAME,
                                           BASIC_SEARCH_PROVIDER_OBJECT_PATH,
                                           DBUS_TIMEOUT_MS,
                                           "Basic search provider");

  g_autoptr (GObject) dummy = g_object_new (G_TYPE_OBJECT, NULL);
  g_object_set_data_full (dummy, "worker_tx", g_async_queue_ref (fixture->worker_tx), (GDestroyNotify) g_async_queue_unref);

  worker = _search_dbus_worker_thread_start (main_context,
                                             fixture->worker_rx,
                                             fixture->worker_tx,
                                             fixture->force_stop,
                                             fixture->query,
                                             fixture->result_list,
                                             100 * G_TIME_SPAN_MILLISECOND, /* retrigger interval */
                                             config,
                                             message_ready_cb,
                                             dummy,
                                             &error);
  g_assert_no_error (error);

  g_thread_join (worker);
}

gint
main (gint argc, gchar *argv[])
{
  g_autoptr (GTestDBus) test_dbus = NULL;

  g_test_init (&argc, &argv, NULL);

  g_test_add (
      "/Search/unit/test_dbus_worker",
      DBusWorkerFixture, NULL, setup, test_dbus_worker, teardown);

  test_dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (test_dbus);

  return g_test_run ();
}
