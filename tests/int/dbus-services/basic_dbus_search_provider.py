#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

"""A basic D-Bus search provider, used for testing `Search.ProviderDBus`."""


from gi.repository import GLib
from gi.repository import Gio

# We use pydbus as GDBus can't publish services from Python,
# according to https://github.com/hcoin/pydbus
import pydbus
import pydbus.generic

import argparse
import configparser
import logging
import pathlib
import sys
from dataclasses import dataclass
from typing import *

log = logging.getLogger()

DBUS_NAME = "org.gnome.gitlab.sthursfield.libgnomesearch.BasicSearchProvider"


@dataclass
class Document:
    identifier: str
    text: str


SIMPLE_SEARCH_CORPUS = [
    Document(
        identifier="1",
        text="The capital city of Australia is Canberra.",
    ),
    Document(
        identifier="2",
        text="Plants convert sunlight into energy through the process of photosynthesis.",
    ),
    Document(
        identifier="3",
        text="The first person to set foot on the moon was Neil Armstrong.",
    ),
    Document(
        identifier="4",
        text="Supply and demand is a concept in economics that relates to the relationship between the availability of a product or service and the desire for it.",
    ),
    Document(
        identifier="5",
        text="The main causes of climate change and global warming include greenhouse gas emissions, deforestation, and industrial activities.",
    ),
    Document(
        identifier="6",
        text="Cellular respiration is the process by which living organisms convert glucose and oxygen into energy, carbon dioxide, and water.",
    ),
    Document(
        identifier="7",
        text="The human nervous system has major functions such as transmitting signals, coordinating bodily functions, and processing sensory information.",
    ),
    Document(
        identifier="8",
        text="The Mona Lisa was painted by Leonardo da Vinci and is considered one of the most famous and influential artworks in history.",
    ),
]


class BasicSearch:
    """A very basic search implementation."""

    def document_contains_terms(self, document: Document, terms: List[str]) -> bool:
        for term in terms:
            if term not in document.text:
                return False
        return True

    def match_documents(self, corpus: List[Document], terms: List[str]) -> List[Document]:
        results = []
        for doc in corpus:
            if self.document_contains_terms(doc, terms):
                results.append(doc)
        return results


class SearchProvider2:
    """<node>
        <interface name="org.gnome.Shell.SearchProvider2">
            <method name="GetInitialResultSet">
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetSubsearchResultSet">
                <arg type="as" name="previous_results" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetResultMetas">
                <arg type="as" name="identifiers" direction="in" />
                <arg type="aa{sv}" name="metas" direction="out" />
            </method>
            <method name="ActivateResult">
                <arg type="s" name="identifier" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
            <method name="LaunchSearch">
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
        </interface>
    </node>"""

    def __init__(self, main_loop: GLib.MainLoop):
        self.main_loop = main_loop
        self.test_icon = Gio.ThemedIcon.new("com.example.TestIcon")
        self._basic_search = BasicSearch()

    def GetInitialResultSet(self, terms: List[str]) -> List[str]:
        log.info("Initial search for %s", str(terms))
        self.main_loop.reset_active_timeout()
        results = self._basic_search.match_documents(SIMPLE_SEARCH_CORPUS, terms)
        return [result.identifier for result in results]

    def GetSubsearchResultSet(self, previous_results: List[str], terms: List[str]):
        log.info("Subsearch for %s", str(terms))
        self.main_loop.reset_active_timeout()

        corpus = [
            doc for doc in SIMPLE_SEARCH_CORPUS if doc.identifier in previous_results
        ]
        results = self._basic_search.match_documents(corpus, terms)
        return [result.identifier for result in results]

    def GetResultMetas(self, results: List[str]):
        log.info("Get result metas for %s", results)
        self.main_loop.reset_active_timeout()

        metas = []
        for doc in SIMPLE_SEARCH_CORPUS:
            if doc.identifier in results:
                name = f"Document {doc.identifier}"
                description = doc.text
                metas.append(
                    {
                        "id": GLib.Variant("s", doc.identifier),
                        "name": GLib.Variant("s", name),
                        "gicon": GLib.Variant("s", self.test_icon.to_string()),
                        "description": GLib.Variant("s", description),
                    }
                )
        return metas

    def ActivateResult(self, result, terms, timestamp):
        log.info("Activate %s", result)
        self.main_loop.reset_active_timeout()
        # Nothing to do here

    def LaunchSearch(self, terms, timestamp):
        log.info("Launch search %s", terms)
        self.main_loop.reset_active_timeout()
        # Nothing to do here


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Fake DBus search provider for testing libgnomesearch"
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        help="Enable detailed logging to stderr",
    )
    parser.add_argument(
        "--timeout",
        metavar="SECONDS",
        default=10,
        help="Shut down process after SECONDS inactivity. Default: 10)",
    )
    return parser


class MainLoop:
    """Wrapper around GLib main loop which adds an inactivity timeout."""

    def __init__(self):
        self.loop = GLib.MainLoop()

        self.timeout = None
        self.timeout_id = None

    def set_inactive_timeout(self, seconds=None):
        self.timeout = seconds
        self.reset_active_timeout()

    def reset_active_timeout(self):
        if self.timeout_id:
            GLib.source_remove(self.timeout_id)
            self.timeout_id = None
        if self.timeout:
            GLib.timeout_add_seconds(self.timeout, lambda: self._inactive_timeout())

    def _inactive_timeout(self):
        log.info("Exiting due to %i seconds inactivity timer", self.timeout)
        self.loop.quit()
        return GLib.SOURCE_REMOVE

    def run(self):
        self.loop.run()


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    loop = MainLoop()

    bus = pydbus.SessionBus()
    bus.publish(DBUS_NAME, SearchProvider2(loop))

    log.info("Waiting for requests on D-Bus name %s", DBUS_NAME)
    loop.set_inactive_timeout(int(args.timeout))
    loop.run()


if __name__ == "__main__":
    try:
        main()
    except RuntimeError as e:
        sys.stderr.write("ERROR: {}\n".format(e))
        sys.exit(1)
