# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

"""Integration tests for libgnomesearch using a basic D-Bus search provider.

These tests use a real, private D-Bus instance. The provider searches some
strings hardcoded into the basic_dbus_search_provider.py module.

"""

import gi
gi.require_version('Search', '0.1')
from gi.repository import GLib, Search

from pytest import fixture, mark
from testutils import MainLoop

import logging

LOG = logging.getLogger(__name__)

@fixture
def context_with_basic_dbus_search_provider(private_dbus, tmp_path):
    basic_provider_busname = "org.gnome.gitlab.sthursfield.libgnomesearch.BasicSearchProvider"
    basic_provider_object_path = "/org/gnome/gitlab/sthursfield/libgnomesearch/BasicSearchProvider"

    providers_dir = tmp_path.joinpath("search-providers")
    providers_dir.mkdir()
    basic_provider_config = providers_dir.joinpath(basic_provider_busname).with_suffix(".ini")
    basic_provider_config.write_text("\n".join([
        "[Shell Search Provider]",
        "BusName=" + basic_provider_busname,
        "ObjectPath=" + basic_provider_object_path,
        "DesktopId=no-app.desktop",
        "Version=2",
    ]))

    context = Search.Context()
    context.set_dbus_connection(private_dbus)
    context.set_search_provider_dir(str(providers_dir))
    return context


def test_basic(context_with_basic_dbus_search_provider):
    """Test that the basic D-Bus provider is loaded correctly."""
    context = context_with_basic_dbus_search_provider

    providers = context.load_providers(False)
    assert len(providers) == 1


def run_search_and_wait(context: Search.Context, search: Search.Group, cancel_msec=None):
    main_loop = MainLoop()

    def stopped_cb(search: Search.Group, elapsed_time: float, error: GLib.Error):
        main_loop.quit()

    def cancel_cb(*args):
        search.force_stop()
        return GLib.SOURCE_REMOVE

    if cancel_msec is not None:
        GLib.timeout_add(cancel_msec, cancel_cb)

    search.connect("stopped", stopped_cb)
    search.start()
    main_loop.run_checked()


def test_oneshot_search(context_with_basic_dbus_search_provider):
    """Test a oneshot search using the basic D-Bus provider."""
    context = context_with_basic_dbus_search_provider

    query = Search.Query.new_text("process")
    search = context.create_oneshot_search(query)

    run_search_and_wait(context, search)

    result_lists = search.get_result_lists()
    assert len(result_lists) == 1
    assert result_lists[0].get_n_items() == 3


@mark.parametrize('cancel_msec', [0, 10, 25, 100, 250])
def test_oneshot_search_cancel(context_with_basic_dbus_search_provider, cancel_msec):
    """Test a oneshot search that gets cancelled."""
    context = context_with_basic_dbus_search_provider

    query = Search.Query.new_text("process")
    search = context.create_oneshot_search(query)

    try:
        run_search_and_wait(context, search, cancel_msec)
    except GLib.Error as exc:
        # If the cancel timoeut fires before the query finishes, we will get a
        # cancelled exception. Any other exception type is a test failure.
        assert 'cancelled' in exc.message

    # It's also valid if the _finish callback fires before the cancellation
    # happened.

    # We don't know what state the ResultList will be in, as it depends on
    # timing in other threads whether the search already produced results.


def test_typeahead_search(context_with_basic_dbus_search_provider):
    """Test a typeahead search using the basic D-Bus provider."""
    context = context_with_basic_dbus_search_provider

    query = Search.Query.new_text("process")
    search = context.create_typeahead_search(query)

    run_search_and_wait(context, search)

    result_lists = search.get_result_lists()
    assert len(result_lists) == 1
    assert result_lists[0].get_n_items() == 3
