# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

import sys

from gi.repository import GLib

class MainLoop():
    '''Wrapper for GLib.MainLoop that propagates any unhandled exceptions.

    PyGObject doesn't seem to provide any help with propagating exceptions from
    the GLib main loop to the main Python execution context. The default
    behaviour is to print a message and continue, which is useless for tests as
    it means tests appear to pass when in fact they are broken.

    '''

    def __init__(self):
        self._loop = GLib.MainLoop.new(None, 0)

    def quit(self):
        self._loop.quit()

    def run(self):
        raise NotImplemented("Use .run_checked() to ensure correct exception handling.")

    def run_checked(self):
        '''Run the loop until quit(), then raise any unhandled exception.'''
        self._exception = None

        old_hook = sys.excepthook

        def new_hook(etype, evalue, etb):
            self._loop.quit()
            self._exception = evalue
            old_hook(etype, evalue, etb)

        try:
            sys.excepthook = new_hook
            self._loop.run()
        finally:
            sys.excepthook = old_hook

        if self._exception:
            raise self._exception
