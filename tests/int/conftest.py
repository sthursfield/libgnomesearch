# SPDX-FileCopyrightText: 2023  Sam Thursfield
# SPDX-License-Identifier: GPL-3.0-or-later

"""Built-in fixtures for the libgnomesearch integration tests."""

from pytest import fixture

import os

from testutils import DBusDaemon


@fixture
def private_dbus():
    # This is set by Meson to point at the generated test-bus.conf file,
    # which configures `dbus-daemon` to load D-Bus services from the build
    # tree.
    config = os.environ['TEST_BUS_CONFIG']

    daemon = DBusDaemon(config)
    daemon.start()
    conn = daemon.get_connection()
    try:
        yield conn
    finally:
        daemon.stop()
