#!/bin/sh

# Check if builddir and example executable exist
if [ ! -d "builddir" ] || [ ! -f "builddir/examples/gnomesearch_cli" ]; then
  echo "builddir or example executable not found. Run build_everything.sh before running the example."
  exit 1
fi

# Run the example
builddir/examples/gnomesearch_cli