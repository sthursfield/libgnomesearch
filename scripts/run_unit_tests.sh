#!/bin/sh

# Check if builddir exists
if [ ! -d "builddir" ]; then
  echo "builddir not found. Run a build script before running tests."
  exit 1
fi

# Run unit tests
meson test -C builddir