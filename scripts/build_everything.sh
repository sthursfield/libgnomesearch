#!/bin/sh

# Check if meson.build exists
if [ ! -f "meson.build" ]; then
  echo "meson.build not found. Are you in the project root directory?"
  exit 1
fi

set -e

BUILD_DIR="builddir"

echo "Removing the build directory..."
if [ -d "$BUILD_DIR" ]; then
  rm -rf "$BUILD_DIR"
fi

echo "Creating and configuring build directory..."
meson setup "$BUILD_DIR" --buildtype=release

echo "Building the project..."
ninja -C "$BUILD_DIR"

echo "Running tests..."
ninja -C "$BUILD_DIR" test

echo "Installation..."
sudo ninja -C "$BUILD_DIR" install