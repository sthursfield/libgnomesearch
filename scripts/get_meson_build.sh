#!/bin/bash

output_file="combined_meson.build"

# Remove the output file if it already exists
if [ -f "$output_file" ]; then
  rm "$output_file"
fi

# Find all meson.build files, concatenate and remove empty lines
find . -name "meson.build" -type f -exec cat {} \; | sed '/^\s*$/d' > "$output_file"

echo "All meson.build files have been concatenated into $output_file"
