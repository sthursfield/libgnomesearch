#!/bin/bash

# Check if the script is run from the project's root directory
if [ ! -f meson.build ]; then
    echo "Please run this script from the project's root directory."
    exit 1
fi

# Find all .c and .h files in the project directory
FILES=$(find . -type f -name '*.c' -o -name '*.h')

# Use clang-format to format the code
echo "Applying clang-format..."
for FILE in $FILES; do
    clang-format -i -style=file $FILE
done

# Use clang-tidy with the .clang-tidy config file found in the project directory or its parent directories
echo "Running clang-tidy..."
clang-tidy --quiet $FILES -p=builddir
